/******************************************************************************
* Filename:       cartridge.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains all functions associated with interfacing to the 
*                   1-wire NVRAM on the light cartridge
*                       
* List of Functions:  uint16 ccitt(uint8 *data, uint8 len) 
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"


// Static Function Declarations
#ifdef EEPROM_COMM
static brisa_temp_comp_params_t temp_comp_data;
static brisa_pulse_params_t pulse_data;

static uint8 data_buf[40];
static uint16 read_crc;
static uint16 crc_temp;

#endif


/******************************************************************************
* function:    void ccitt(void)
*
* description: Calcuates the 16-bit CCITT CRC on the passed data.
*
* passed:      data - pointer to the first character of the data to be processed
*              len - length of the data to be processed
*
* returns:     calaculated 16-bit CRC
*
******************************************************************************/
uint16 ccitt(uint8 *data, uint8 len)
{
    uint16 crc = 0xffff;
    
    while(len--)
    {
        crc  = (uint8)(crc >> 8) | (crc << 8);
        crc ^= *data++;
        crc ^= (uint8)(crc & 0xff) >> 4;
        crc ^= (crc << 8) << 4;
        crc ^= ((crc & 0xff) << 4) << 1;
    }    
    
    return crc;
}

/******************************************************************************
* function:    uint8 eepromReadBlock(uint8 device,uint16 blockAddress, uint8 *data, uint16 blockSize )
*
* description: Read a CRCed block of data.
*
* passed:      blockAddress - Cartridge memory data address of the first byte of data
*			   data - pointer to memory to load the data into
*              len - length of the data buffer to fill
*
* returns:     status
*
******************************************************************************/

uint8 eepromReadBlock(uint8 device, uint16 blockAddress, uint8 *data, uint16 blockSize, bool do_crc  )
{
    
#ifdef EEPROM_COMM
uint8 i;
uint8 i2c_status;
uint8 loop_end;


    loop_end = blockSize;
    if( do_crc == TRUE )
    {
        loop_end += 2;
    }
    // Read The Data Stored In NVRAM    
    for( i = 0; i < loop_end; i++ )
    {
        i2c_status = i2cEepromByteRead(device, blockAddress+i,&data_buf[i]);
        if( i2c_status != NO_ERROR )
        {
            return(i2c_status);
        }
    }

    if( do_crc == TRUE )
    {
        crc_temp = ccitt( data_buf, (uint8)blockSize);
        read_crc = 0;
        read_crc = data_buf[blockSize];
        read_crc |= (data_buf[blockSize+1] << 8);

        if( crc_temp != read_crc )
        {
            return(ERROR_CAP_DATA_CORRUPT);
        }
    }
    // Read The Data Stored In NVRAM    
    for( i = 0; i < blockSize; i++ )
    {
        *(data+i) = data_buf[i];
    }
#endif

    return (NO_ERROR);

}


/******************************************************************************
* function:    uint8 load_cap_params(uint16 base_address, gen2_cart_data_info_t *data)
*
* description: Reads the Gen II global data section from the cap
*
* passed:      gen2 cartridge data structure (output)
*
* returns:     0 - successful
*              n - failed error code
******************************************************************************/
uint8 load_cap_params( gen2_cart_data_info_t *data)
{
    uint8 status=NO_ERROR;

#ifdef EEPROM_COMM

    status = eepromReadBlock(CAP_EEPROM_DEVICE, TEMP_COMP_ADDR, (uint8 *)&temp_comp_data,
        sizeof(brisa_temp_comp_params_t), TRUE);
    if( status == NO_ERROR )
    {
        data->tubeTempComp = temp_comp_data.tube_comp;
        data->tubeTempCompFlashUpdate = temp_comp_data.flash_update;
        data->tubeTempCompCooldown1 = temp_comp_data.cooldown1;
        data->tubeTempCompCooldown2 = temp_comp_data.cooldown2;
        data->express_comp = temp_comp_data.express_comp;
        data->express_update = temp_comp_data.express_update;
        status = eepromReadBlock(CAP_EEPROM_DEVICE, PULSE_DATA_ADDR, (uint8 *)&pulse_data,
            sizeof(brisa_pulse_params_t), TRUE);
	if( status == NO_ERROR )
	{
            data->pulsePowerSumLimit = pulse_data.sum_limit;
            data->pulseFlashTime_ms = pulse_data.time;
            data->pulseFlashRate_ms = pulse_data.rate;
            data->bypassDutyCycle = pulse_data.Duty;
            data->expressFlashRate_ms = pulse_data.expressRate;
	}
    }
#else
    data->tubeTempComp = 1;
    data->tubeTempCompFlashUpdate = 5750;
    data->tubeTempCompCooldown1 = 40;
    data->tubeTempCompCooldown2 = 15;
    data->express_comp = 50;
    data->express_update = 6000;
    data->pulsePowerSumLimit = 200000;
    data->bypassDutyCycle = 57;
    data->pulseFlashTime_ms = 35;
    data->pulseFlashRate_ms = 3000;
#endif

    return( status);
}


/******************************************************************************
* function:    void store_uint32(uint8 device,uint16 addr, uint32 val)
*
* description: Writes 32 bit value to the specifed cartridge memory address.
*
* passed:      addr - address to write the data to
*			   val - 32 bit value to write
*
* returns:     none
*
******************************************************************************/
void store_uint32(uint8 device, uint16 addr, uint32 val)
{
#ifdef EEPROM_COMM
uint8 write_buf[20];
uint8 i;

    write_buf[0] = (uint8)(val & 0x00ff);
    write_buf[1] = (uint8)((val >> 8) & 0x00ff);
    write_buf[2] = (uint8)((val >> 16) & 0x00ff);
    write_buf[3] = (uint8)((val >> 24) & 0x00ff);

    for(i = 0; i < 4; i++ )
    {
        i2cEepromWrite(device, addr+i,write_buf[i]);
    }
#endif
}




