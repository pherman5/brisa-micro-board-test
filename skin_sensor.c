/******************************************************************************
* Filename:       skin_sensor.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains the skin sensor functionality
*                       
* List of Functions:
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"

// Definitions For Development Purposes
// Outputs Temperature Information And Signals To RS232 Port


// Average Number Samples For Measurments
#define NUM_SENSOR_AVERAGE                  5

#define STT_PD_DARK_THRESH      100
#define STT_PD_GREEN_THRESH     0x2000
#define STT_PD_AMBER_THRESH     0x1000

/******************************************************************************
* function:    uint8 read_skin_sensor_data( int16 *sts1_green_val, int16 *sts1_amber_val,
*        int16 *sts2_green_val, int16 *sts2_amber_val)
*
* description: Measures the skin sensor reflectance.
*
* passed:      sts1_green_val - A pointer to return the green light - green dark signal for sts1
*              sts1_amber val - A pointer to return the amber light - amber dark signal for sts1
*              sts2_green_val - A pointer to return the green light - green dark signal for sts2
*              sts2_amber val - A pointer to return the amber light - amber dark signal for sts2
*
* returns:     status
*
******************************************************************************/
uint8 read_skin_sensor_data( int16 *sts1_green_val, int16 *sts1_amber_val,
        int16 *sts2_green_val, int16 *sts2_amber_val)
{
    int16 ad_value;
    uint8 num_light_loops;
    uint8 i,j;
    int32 sts1GreenLightSignal, sts1GreenDarkSignal;
    int32 sts1AmberLightSignal, sts1AmberDarkSignal;
    int32 sts2GreenLightSignal, sts2GreenDarkSignal;
    int32 sts2AmberLightSignal, sts2AmberDarkSignal;

#define LED_SETTLE_TIME_USECS 170


    num_light_loops = 10;

    ICD_CLK_OFF;

//    sleep( 10 );
    sts1GreenLightSignal = 0;
    sts1GreenDarkSignal = 0;
    sts2GreenLightSignal = 0;
    sts2GreenDarkSignal = 0;
    STS_AMBER_LED_OFF;
    STS_GREEN_LED_OFF;

    for( i = 0; i < num_light_loops; i++ )
    {
        if (stt_flag_val & 0x01)  STS_GREEN_LED_ON;
        
        __delay_us( LED_SETTLE_TIME_USECS);

            ICD_CLK_ON;
            
        for( j = 0; j < NUM_SENSOR_AVERAGE; j++ )
        {
            // do adc read
            adc12_read_channel(ADC_STS1_SIG, &ad_value, TRUE );
            sts1GreenLightSignal += (int32)ad_value;
            adc12_read_channel(ADC_STS2_SIG, &ad_value, TRUE );
            sts2GreenLightSignal += (int32)ad_value;
        }
              
            ICD_CLK_OFF;

        STS_GREEN_LED_OFF;
        __delay_us( LED_SETTLE_TIME_USECS);

            ICD_CLK_ON;
            
        for( j = 0; j < NUM_SENSOR_AVERAGE; j++ )
        {
            // do adc read
            adc12_read_channel(ADC_STS1_SIG, &ad_value, TRUE );
            sts1GreenDarkSignal += ad_value;
            adc12_read_channel(ADC_STS2_SIG, &ad_value, TRUE );
            sts2GreenDarkSignal += ad_value;
        }
            
            ICD_CLK_OFF;

    }

    *sts1_green_val = (int16)((sts1GreenLightSignal - sts1GreenDarkSignal) / num_light_loops);
    *sts2_green_val = (int16)((sts2GreenLightSignal - sts2GreenDarkSignal) / num_light_loops);


    sts1AmberLightSignal = 0;
    sts1AmberDarkSignal = 0;
    sts2AmberLightSignal = 0;
    sts2AmberDarkSignal = 0;
    STS_GREEN_LED_OFF;

    for( i = 0; i < num_light_loops; i++ )
    {
        if(stt_flag_val & 0x02) STS_AMBER_LED_ON;
        
        __delay_us( LED_SETTLE_TIME_USECS);

        ICD_CLK_ON;
        
	for( j = 0; j < NUM_SENSOR_AVERAGE; j++ )
	{
                // do adc read
            adc12_read_channel(ADC_STS1_SIG, &ad_value, TRUE );
            sts1AmberLightSignal += ad_value;
            adc12_read_channel(ADC_STS2_SIG, &ad_value, TRUE );
            sts2AmberLightSignal += ad_value;
	}
         
        ICD_CLK_OFF;

        STS_AMBER_LED_OFF;
        __delay_us( LED_SETTLE_TIME_USECS);
		
        ICD_CLK_ON;
        
	for( j = 0; j < NUM_SENSOR_AVERAGE; j++ )
	{
                // do adc read
            adc12_read_channel(ADC_STS1_SIG, &ad_value, TRUE );
            sts1AmberDarkSignal += ad_value;
            adc12_read_channel(ADC_STS2_SIG, &ad_value, TRUE );
            sts2AmberDarkSignal += ad_value;
	}
         
        ICD_CLK_OFF;

    }

    *sts1_amber_val = (int16)((sts1AmberLightSignal - sts1AmberDarkSignal) / num_light_loops);
    *sts2_amber_val = (int16)((sts2AmberLightSignal - sts2AmberDarkSignal) / num_light_loops);

    return(NO_ERROR);
}

/******************************************************************************
* function:    uint8 check_stt(void)
*
* description: Loads the puck data including cap type
*
* passed:      void
*
* returns:     status
*
******************************************************************************/
uint8 check_stt(void)
{
uint8 flash_val, alert;
int16 stt1_g,stt1_a,stt2_g,stt2_a;
int16 threshold;

        read_skin_sensor_data(&stt1_g,&stt1_a,&stt2_g,&stt2_a);
        flash_val = 0;
        switch (stt_flag_val)
        {
            case 0:
                threshold = STT_PD_DARK_THRESH;
                break;
                
            case 1:
                threshold = STT_PD_GREEN_THRESH;
                break;
                
            case 2:
                threshold = STT_PD_AMBER_THRESH;
                break;
        }
        
        if(stt1_g > threshold) flash_val |= 0x01;
        if(stt2_g > threshold) flash_val |= 0x02;
        if(stt1_a > threshold) flash_val |= 0x04;
        if(stt2_a > threshold) flash_val |= 0x08;
        
        switch (stt_flag_val)
        {
            case 0:
                if (flash_val != 0)
                {
                    alert = 1;
                    state_unfixable_error(ERROR_STT_DARK_CURRENT_TOO_HIGH);
                }
                else
                {
                    alert = 0;
                    flash_val = 0x10;  // dark current OK flash some light
                }
                break;
                
            case 1:
                if (flash_val != 3)
                {
                    alert = 1;
                    state_unfixable_error(ERROR_STT_GREEN_CURRENT_TOO_LOW);
                }
                else
                    alert = 0;
                break;
                
            case 2:
                if (flash_val != 12)
                {
                    alert = 1;
                    state_unfixable_error(ERROR_STT_AMBER_CURRENT_TOO_LOW);
                }
                else
                    alert = 0;
                break;
        }
        
        Flash_Ecode(flash_val,alert);
        
        return flash_val;
}