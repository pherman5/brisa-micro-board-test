/******************************************************************************
* Filename:       utils.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains utilitie routines.
*                       
* List of Functions:   void utils_enable_interrupts(void)
*                      void utils_disable_interrupts(void)
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"


#define FAN_EDGE_TIMEOUT             75

/******************************************************************************
* function:    void utils_enable_interrupts(void)
*
* description: Sets the global interrupt enable bit which enables all interrupts 
*              who have their periperhal enable bits enabled. 
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void utils_enable_interrupts(void)
{
    INTCON = INTCON | 0xC0;
}    

/******************************************************************************
* function:    void utils_disable_interrupts(void)
*
* description: Clears the global interrupt enable bit which disables all interrupts 
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void utils_disable_interrupts(void)
{
    INTCON = INTCON & 0x3F;
}


/******************************************************************************
* function:    uint8 transitionState(uint8 current_state, uint8 next_state)
*
* description: This routine transitions from the current state to the next state
*               it does teh approprate exit and entry functions
*
* passed:      current_state - The current running state
*               next_state - The state to transition to
*
* returns:     the next state
*
******************************************************************************/
uint8 transitionState(uint8 current_state, uint8 next_state)
{
    //*******************************
    // By checking state here we can do an exit fuction
    //*******************************
    if( current_state != next_state )
    {
        switch( current_state )
        {
            case MAIN_STATE_IDLE:
                stop_led_blink();
                STS_AMBER_LED_OFF;
                break;

            default:
                break;
        }
    }


    //*******************************
    // Enter The Next State, entry function
    //*******************************
    switch( next_state )
    {

        case MAIN_STATE_IDLE:
            main_idle_entry();
            notification(NOTIFY_IDLE,0);
            break;

        case MAIN_STATE_READY:
            notification(NOTIFY_IN_CONTACT_AND_READY,0);
            break;

        case MAIN_STATE_INACTIVITY_TIMEOUT:
            notification(NOTIFY_INACTIVITY,0);
            break;

        default:
            break;
    }

    return(next_state);
}

/******************************************************************************
* function:    void reboot(void)
*
* description: This routine vectors program control to the entry point of
*               the bootloader
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void reboot(void)
{

    I2C_POWER_OFF;
    sleep(500);
    I2C_POWER_ON;
    sleep(100);
    utils_disable_interrupts();
    WDTCON = WDT_OFF | WDT_250MS_TIMEOUT;
    BTLDR();
}