/******************************************************************************
* Filename:       ui.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains all functions associated with the:
*                   leds:  overtemp, power level and gauge
*                   ui buttons: power level select
*                   switch closures: activation, door ajar and contact switches
*                   beeper sounds
*                       
* List of Functions:   void ui_init(void);
*                      void ui_update_overtemp_led(uint8 state);
*                      void ui_update_power_level(uint8 power_level);
*                      void ui_update_power_leds(uint8 value);
*                      void ui_update_gauge_led(uint8 state);
*                      void ui_leds_all_on(void);
*                      void ui_leds_all_off(void);
*					   void ui_leds_enable_blink(uint8 leds_to_blink, uint16 period_ms);
*                      void ui_update_skin_sensor_leds(uint8 state);
*                      void ui_clock_out_led_data(uint8 data);
*                      void ui_update_switch_states(uint8 *current, uint8 *previous);
*                      uint8 ui_read_switch_states( void);
*					   void ui_beep_tune(uint8 tuneID, uint8 repeat);
*					   void ui_update_beeper(void);
*					   void ui_update_leds(void);
*                    
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"

#define USER_REQUEST_DEBOUNCE_TIME_MSECS            50

#define LED_OFF_CMD     0
#define LED_BLINK_CMD   1
#define LED_SEQ_CMD     2
#define LED_PULSE_CMD   3

static uint16 beep_duration;
static uint8 beep_kickstart;

static uint16 led_blink_mask=0;
static uint16 on_time_counts;
static uint16 off_time_counts;
static uint16 blink_counter;
static bool blink_leds_on;
static uint8 led_command;
static uint8 led_seq_counter;

static bool pulse_is_rising;
static uint16 pulse_rise_counts;
static uint16 pulse_fall_counts;
static uint8 last_pulse_duty_cycle;

static uint8 previousSwitchState= 0;
static uint32 userRequestDebounceTimeMsecs=0;

#define UNIT_NOT_USED_EXPIRED_TIME_MSECS	300000		// 5 Minutes

static uint32 unit_not_used_time_msecs;

/******************************************************************************
* These Variables And Structures Are Used For Playing Tunes On The Beeper.
******************************************************************************/
// Make sure that the each tune has no more than this maximum number of tones
#define MAX_NUMBER_TUNE_TONES       20

// Repeat Flag For Playing Tunes Continuously
static uint8 beepTuneRepeat;

// Index To Current Tone In Tune Being Played
static uint8 toneIndex;

// Number Of Tones To Be Played For A Tune
static uint8 numberTuneTones;

// Structure Definition For Various Tunes
struct tune
{
   uint16 frequency;
   uint16 duration_ms;
};

// Unit Ready To Flash Tune
static const struct tune UnitReadyToFlash[] =
{
	3000,		25,
	2500,		25,
	3000,		25,
};

// Unit Not Ready To Flash Tune

static const struct tune UnitNotReadyToFlash[] =
{
	500,		125,
	0,		50,
	500,		125,
	0,		50,
};

// Skin Sensor Calibration Completed Tune
static const struct tune SkinSensorCalComplete[] =
{
	3000,		25,
	2500,		25,
	3000,		25,
        0,              2000,
};

// Skin Sensor Pass Tune
static const struct tune OutOfCooldown[] =
{
	1000,		100,
	0,		75,
	2000,		100,
};

// Cap Removed Tune
static const struct tune CapRemoved[] =
{
	2000,		50,
	0,		50,
	2000,		50,
	0,		50,
	2000,		50,
	0,		50,
	2000,		50,
//	0,		500,
};

// Over Temp Shutdown Tune
static const struct tune OverTempShutdown[] =
{
	1000,		75,
	1500,		75,
	2000,		75,
	0,		1500,
};

// Unrecoverable Error Tune
static const struct tune UnrecoverableError[] =
{
	600,		250,
	0,		100,
	500,		250,
//	500,		100,
};

static const struct tune InactivityAnoyingTune[] =
{
	1000,		400,
	0,		150,
        600,            400,
        0,              150
};

static const struct tune TheFifthTune[] =
{
	300,		400,
	0,		70,
        300,            400,
        0,              70,
        300,            400,
        0,              70,
        200,            800,
        0,              500,
	250,		400,
	0,		70,
        250,            400,
        0,              70,
        250,            400,
        0,              70,
        150,            800,
        0,              1000
};


// These Arrays Have Been Defined Because Of A Compiler Issue With Using
// A Reference To A Pointer Within An Interrupt. Instead Of Accessing The
// Above Tune Structures Via A Pointer, The Information Must Be Copied Into
// These Arrays So That The ISR Can Access Them. Hopefully This Can Be Resolved
// In Newer Versions Of The Compiler.
// NOTE: The Size Of These Arrays Must Be The Maximum Size Of The Tunes Defined Above
static uint16 tuneFrequency[MAX_NUMBER_TUNE_TONES];
static uint16 tuneDurationTicks[MAX_NUMBER_TUNE_TONES];

/******************************************************************************
* function:    void ui_init(void)
*
* description: Initializes the variables associated with the ui drivers
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void ui_init(void)
{
    // Initialize Beeper Variables
    beep_duration = 0;
    beep_kickstart = FALSE;
    led_command = LED_OFF_CMD;
    unit_not_used_time_msecs = 0;
    all_user_leds_off();
}


/******************************************************************************
* function:    void ui_beep_tune( uint8 tuneID, unit8 repeat)
*
* description: Initiatiates the playing of the tune requested.  
*
* passed:      tuneID - Tune ID Requested
*              repeat - TRUE or FALSE Repeats Tune Continuosuly Until Stopped
*
* returns:     none
*
******************************************************************************/
void ui_beep_tune( uint8 tuneID, uint8 repeat)
{
    if( baseDiagConfig.DisableSound == FALSE)
    {
        struct tune const *ptrTune;
    	
    	// Turn Off Any Active Tune
        stop_buzzer();
    
    	// Establish Whether Tune Should Repeat Continuously Until Manually Stopped
    	// Or If Tune Should Stop Automatically After One Play
    	beepTuneRepeat = repeat;
    
    	// Initialize The Tone Index To The Start Of The Tune
    	toneIndex = 0;
    
    	switch( tuneID)
    	{
            case UNRECOVERABLE_ERROR:
    		ptrTune = UnrecoverableError;
    		numberTuneTones = sizeof( UnrecoverableError) / sizeof( struct tune);
    		break;

            case INACTIVITY_TUNE:
    		ptrTune = InactivityAnoyingTune;
    		numberTuneTones = sizeof( InactivityAnoyingTune) / sizeof( struct tune);
    		break;

            default:
    		return;		// Do Not Process If Invalid Tune Requested
    	}

        // Precalculate The Beeper Prescaler, Frequency Counts, and Duration Ticks. Store Values For Use By Update Beeper Routine
    	for( int i = 0; i < numberTuneTones; i++)
    	{
            if( (ptrTune[i].frequency < MIN_BEEPER_FREQUENCY) || (ptrTune[i].frequency > MAX_BEEPER_FREQUENCY))
            {
                tuneFrequency[i] = 0;
            }
            else
            {
                tuneFrequency[i] = ptrTune[ i].frequency;
            }
    
            tuneDurationTicks[ i] = ptrTune[ i].duration_ms / GLOBAL_TIME_TICK;
    	}
    
        // Kick Start The Beeper
        beep_kickstart = TRUE;
    }
}

/******************************************************************************
* function:    void ui_beep_tune_stop( void)
*
* description: Stops The Playing Of A Tune.  
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void ui_beep_tune_stop( void)
{
    beep_duration = 0;
    beepTuneRepeat = FALSE;
    stop_buzzer();
}

/******************************************************************************
* function:    void ui_update_beeper( void)
*
* description: Updates Beeper
*			   NOTE: This Routine Is Called From The Interrupt general_isr()
*                    And Should Not Be Called From Any Other Location.
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void ui_update_beeper( void)
{
    if( beep_kickstart)
    {
	    // Set Up The Initial Beep Duration And Frequency
        beep_duration = tuneDurationTicks[ 0];
        start_buzzer(tuneFrequency[ 0]);
        beep_kickstart = FALSE;

        return;
    }

    // Process Beeper Tunes
    if(beep_duration)
    {
	if(--beep_duration == 0)
	{
            ++toneIndex;
		
            if( (toneIndex >= numberTuneTones) && beepTuneRepeat == FALSE)
            {
                stop_buzzer();
            }
            else
            {
                if( toneIndex >= numberTuneTones)
		{
                    // Repeating tone so set index back to 0
                    toneIndex = 0;
		}
		
		// Reload New Frequency, Duty Cycle, And Duration For Next Tone
		if( tuneFrequency[ toneIndex] == 0)
                {
                    // Play the dead time between repeated tones
                    stop_buzzer();
                }
                else
                {
                    start_buzzer(tuneFrequency[ toneIndex]);
                }

        	beep_duration = tuneDurationTicks[ toneIndex];
            }
	}    
    }
}

/******************************************************************************
* function:    void ui_reset_unit_not_used_expiration_time( void)
*
* description: Resets UI Timer For Unit Not Used
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void ui_reset_unit_not_used_expiration_time(void)
{
    unit_not_used_time_msecs = get_msecs_since_reset() + UNIT_NOT_USED_EXPIRED_TIME_MSECS;
}

/******************************************************************************
* function:    uint8 ui_unit_not_used_time_expired(void)
*
* description: Checks UI Timer For Unit Not Used
*
* passed:      none
*
* returns:     TRUE or FALSE Depending On Whether Or Not Time Has Expired
*
******************************************************************************/
uint8 ui_unit_not_used_time_expired(void)
{
    if( get_msecs_since_reset() >= unit_not_used_time_msecs)
    {
	return(TRUE);
    }
    else
    {
	return FALSE;
    }
}


//******************************************************************************
// Function:    void getUserRequest( void)
//
// Description: This routine handles user requests.
//
// Input:       none
//
// Output:      user request
//
//******************************************************************************
uint8 getUserRequest( uint8 current_state)
{
    #define ACT_SWITCH_ACTIVE_BIT        0x01
    #define EXPRESS_SWITCH_ACTIVE_BIT    0x02
    #define SENSITIVE_SWITCH_ACTIVE_BIT      0x04

    uint8 user_request=USER_REQUEST_NONE;
    uint8 switchState = 0;

    //*******************************************
    // Determine Which Switches Are Asserted
    //*******************************************
    if( ACTIVATION_SWITCH == 0 )
        switchState |= ACT_SWITCH_ACTIVE_BIT;

    if( EXPRESS_SWITCH == 0)
        switchState |= EXPRESS_SWITCH_ACTIVE_BIT;

    if( SENSITIVE_SWITCH == 0)
        switchState |= SENSITIVE_SWITCH_ACTIVE_BIT;

    //*******************************************
    // Debounce The Switch State
    // If No Activity or Switch Changed,
    // Reset User Request and Debounce Timer
    //*******************************************
    if( (switchState == 0) || (switchState != previousSwitchState))
    {
        userRequestDebounceTimeMsecs = get_msecs_since_reset();
	if( switchState == 0 )
	{
            user_request = USER_REQUEST_NONE;
	}
    }

    previousSwitchState = switchState;

    //*******************************************
    // Determine User Requested Action
    //*******************************************
    if( get_msecs_since_reset() >= (userRequestDebounceTimeMsecs  + USER_REQUEST_DEBOUNCE_TIME_MSECS))
    {
        switch( switchState)
        {
            case ACT_SWITCH_ACTIVE_BIT:
                user_request = USER_REQUEST_FLASH;
                break;

            case EXPRESS_SWITCH_ACTIVE_BIT:
                    user_request = USER_REQUEST_EXPRESS;
                break;

            case SENSITIVE_SWITCH_ACTIVE_BIT:
                    user_request = USER_REQUEST_SENSITIVE;
                break;

            case (ACT_SWITCH_ACTIVE_BIT | EXPRESS_SWITCH_ACTIVE_BIT):
                    user_request = USER_REQUEST_EXPRESS;
                break;

            case (SENSITIVE_SWITCH_ACTIVE_BIT | EXPRESS_SWITCH_ACTIVE_BIT):
                user_request = USER_REQUEST_NONE;
                break;

            case (SENSITIVE_SWITCH_ACTIVE_BIT | ACT_SWITCH_ACTIVE_BIT):
                if( current_state == MAIN_STATE_IDLE )
                {
                    user_request = USER_REQUEST_VERSION;
                }
                else
                {
                    user_request = USER_REQUEST_NONE;
                }
                break;

            case (SENSITIVE_SWITCH_ACTIVE_BIT | ACT_SWITCH_ACTIVE_BIT | EXPRESS_SWITCH_ACTIVE_BIT):
                user_request = USER_REQUEST_NONE;
                break;


            default:
                user_request = USER_REQUEST_NONE;
                break;
        }
    }

    //*******************************************
    // Process Special Actions
    //*******************************************
    if( switchState != 0)
    {
        //*******************************************
        // Reset Auto Power Off And Standby Timeouts
        // If User Request Action Occurs
        //*******************************************
        ui_reset_unit_not_used_expiration_time();
    }

    return( user_request);
}


//******************************************************************************
// Function:    void start_led_blink(uint16 led_mask,
//                  uint16 on_time_ms, uint16 off_time_ms)
//
// Description: This routine starts an rgb led blinking.
//
// Input:       color - color for the led
//              led - what led to blink
//              on_time_ms - the time in mSec for the led to be on
//              off_time_ms - the time in mSec for the led to be off
//
// Output:      none
//
//******************************************************************************
void start_led_blink(uint16 led_mask, uint16 on_time_ms, uint16 off_time_ms)
{

    led_blink_mask = led_mask;

    on_time_counts = on_time_ms / LED_SERVICE_TIME_INTERVAL;
    off_time_counts = off_time_ms / LED_SERVICE_TIME_INTERVAL;
    blink_leds_on = TRUE;
    blink_leds_state(TRUE);
    blink_counter = 0;

    led_command = LED_BLINK_CMD;


    return;
}


//******************************************************************************
// Function:    void stop_led_blink(void)
//
// Description: This routine stops an led from blinking and turns it off.
//
// Input:       none
//
// Output:      none
//
//******************************************************************************
void stop_led_blink(void)
{
    led_command = LED_OFF_CMD;
    led_blink_mask = 0;
    blink_leds_state(FALSE);
}


//******************************************************************************
// Function:    void start_led_blink(uint16 led_mask,
//                  uint16 on_time_ms, uint16 off_time_ms)
//
// Description: This routine starts an rgb led blinking.
//
// Input:       color - color for the led
//              led - what led to blink
//              on_time_ms - the time in mSec for the led to be on
//              off_time_ms - the time in mSec for the led to be off
//
// Output:      none
//
//******************************************************************************
void start_led_pulse(uint16 rise_time_ms, uint16 fall_time_ms)
{


    pulse_rise_counts = rise_time_ms / LED_SERVICE_TIME_INTERVAL;
    pulse_fall_counts = fall_time_ms / LED_SERVICE_TIME_INTERVAL;
    blink_counter = 0;

    pulse_is_rising = FALSE;
    led_command = LED_PULSE_CMD;
    last_pulse_duty_cycle = 100;
    start_led_pwm(last_pulse_duty_cycle);

    return;
}


//******************************************************************************
// Function:    void stop_led_blink(void)
//
// Description: This routine stops an led from blinking and turns it off.
//
// Input:       none
//
// Output:      none
//
//******************************************************************************
void stop_led_pulse(void)
{
    stop_led_pwm();
    led_command = LED_OFF_CMD;
    ACT_LED_OFF;
}


//******************************************************************************
// Function:    void start_led_sequence(uint16 sequence_time_ms)
//
// Description: This rotine starts an rgb led blinking.
//
// Input:       color - color for the led
//              led - what led to blink
//              on_time_ms - the time in mSec for the led to be on
//              off_time_ms - the time in mSec for the led to be off
//
// Output:      none
//
//******************************************************************************
void start_led_sequence(uint16 sequence_time_ms)
{


    on_time_counts = sequence_time_ms / LED_SERVICE_TIME_INTERVAL;
    blink_counter = 0;
    PL1_LED_ON;
    PL2_LED_OFF;
    PL3_LED_OFF;
    PL4_LED_OFF;
    PL5_LED_OFF;
    led_seq_counter = 1;
    led_command = LED_SEQ_CMD;


    return;
}


void stop_led_sequence(void)
{
    led_command = LED_OFF_CMD;
    all_user_leds_off();
}



//******************************************************************************
// Function:    void all_leds_off(void)
//
// Description: This routine turns off all leds.
//
// Input:       none
//
// Output:      none
//
//******************************************************************************
void all_leds_off(void)
{
    RED_LED_OFF;
    PL1_LED_OFF;
    PL2_LED_OFF;
    PL3_LED_OFF;
    PL4_LED_OFF;
    PL5_LED_OFF;
    SENSITIVE_LED_OFF;
    stop_led_pulse();
    EXPRESS_LED_OFF;
}

//******************************************************************************
// Function:    void blink_led_handler(void)
//
// Description: This routine handles the pulsing and blinking of the rgb leds.
//              It is called from the main loop every 10 mSec
//
// Input:       none
//
// Output:      none
//
//******************************************************************************
void blink_led_handler(void)
{
    uint8 duty_cycle;

    switch( led_command )
    {
        case LED_OFF_CMD:
            break;

        case LED_BLINK_CMD:
            blink_counter++;
            if( blink_leds_on == TRUE )
            {
                if(blink_counter >= on_time_counts)
                {
                    // LEDS Off
                    blink_leds_state(FALSE);
                    blink_counter = 0;
                    blink_leds_on = FALSE;
                }
            }
            else
            {
                if(blink_counter >= off_time_counts)
                {
                    // LEDS On
                    blink_leds_state(TRUE);
                    blink_counter = 0;
                    blink_leds_on = TRUE;
                }
            }
            break;

        case LED_SEQ_CMD:
            blink_counter++;
            if( blink_counter >= on_time_counts)
            {
                led_seq_counter++;
                if( led_seq_counter > 5 )
                {
                    led_seq_counter = 1;
                }
                all_user_leds_off();
                switch(led_seq_counter)
                {
                    case 1:
                        PL1_LED_ON;
                        break;
                    case 2:
                        PL2_LED_ON;
                        break;
                    case 3:
                        PL3_LED_ON;
                        break;
                    case 4:
                        PL4_LED_ON;
                        break;
                    case 5:
                        PL5_LED_ON;
                        break;
                }
                blink_counter = 0;
            }
            break;

        case LED_PULSE_CMD:
            if( pulse_is_rising )
            {
                blink_counter++;
                if(blink_counter == pulse_rise_counts)
                {
                    blink_counter = 0;
                    pulse_is_rising = FALSE;
                    duty_cycle = last_pulse_duty_cycle;
                }
                else
                {
                    duty_cycle = (blink_counter * 100) / pulse_rise_counts;
                    if( duty_cycle > 100 )
                    {
                        duty_cycle = 100;
                    }
                }
            }
            else
            {
                blink_counter++;
                if( blink_counter == pulse_fall_counts )
                {
                    blink_counter = 0;
                    pulse_is_rising = TRUE;
                    duty_cycle = last_pulse_duty_cycle;
                }
                else
                {
                    duty_cycle = (blink_counter * 100) / pulse_fall_counts;
                    if( duty_cycle > 100 )
                    {
                        duty_cycle = 100;
                    }
                    duty_cycle = 100 - duty_cycle;

                }
            }

            if( duty_cycle != last_pulse_duty_cycle )
            {

                if( duty_cycle != 0)
                {
                    update_led_pwm(duty_cycle);
                }
                else
                {
                    ACT_LED_OFF;
                }

               last_pulse_duty_cycle = duty_cycle;
            }
            break;
    }

    CLRWDT();

}

//******************************************************************************
// Function:    void blink_leds_state(bool state)
//
// Description: This routine sets all the actively
//              blinking leds to the passed state.
//
// Input:       state - on or off state to set leds to
//
// Output:      none
//
//******************************************************************************
void blink_leds_state(bool state)
{
    if( led_blink_mask & RED_LED_BLINK)
    {
        if( state == TRUE )
        {
            RED_LED_ON;
        }
        else
        {
            RED_LED_OFF;
        }
    }

    if( led_blink_mask & PL1_LED_BLINK)
    {
        if( state == TRUE )
        {
            PL1_LED_ON;
        }
        else
        {
            PL1_LED_OFF;
        }
    }

    if( led_blink_mask & PL2_LED_BLINK)
    {
        if( state == TRUE )
        {
            PL2_LED_ON;
        }
        else
        {
            PL2_LED_OFF;
        }
    }

    if( led_blink_mask & PL3_LED_BLINK)
    {
        if( state == TRUE )
        {
            PL3_LED_ON;
        }
        else
        {
            PL3_LED_OFF;
        }
    }

    if( led_blink_mask & PL4_LED_BLINK)
    {
        if( state == TRUE )
        {
            PL4_LED_ON;
        }
        else
        {
            PL4_LED_OFF;
        }
    }

    if( led_blink_mask & PL5_LED_BLINK)
    {
        if( state == TRUE )
        {
            PL5_LED_ON;
        }
        else
        {
            PL5_LED_OFF;
        }
    }

    if( led_blink_mask & ACT_LED_BLINK)
    {
        if( state == TRUE )
        {
            ACT_LED_ON;
        }
        else
        {
            ACT_LED_OFF;
        }
    }

    if( led_blink_mask & FULL_POWER_LED_BLINK)
    {
        if( state == TRUE )
        {
            SENSITIVE_LED_ON;
        }
        else
        {
            SENSITIVE_LED_OFF;
        }
    }

    if( led_blink_mask & EXPRESS_LED_BLINK)
    {
        if( state == TRUE )
        {
            EXPRESS_LED_ON;
        }
        else
        {
            EXPRESS_LED_OFF;
        }
    }

    if( led_blink_mask & STS_AMBER_LED_BLINK)
    {
        if( state == TRUE )
        {
            STS_AMBER_LED_ON;
        }
        else
        {
            STS_AMBER_LED_OFF;
        }
    }

    if( led_blink_mask & STS_GREEN_LED_BLINK)
    {
        if( state == TRUE )
        {
            STS_GREEN_LED_ON;
        }
        else
        {
            STS_GREEN_LED_OFF;
        }
    }
}

//******************************************************************************
// Function:    void display_numeric_value(uint8 value)
//
// Description: This routine displays the 5 bit value of the
//              number passed on the power leds.
//
// Input:       value - the numeric value to display
//
// Output:      none
//
//******************************************************************************
void display_numeric_value(uint8 value)
{
    if(value & 0x01)
    {
        PL1_LED_ON;
    }
    else
    {
        PL1_LED_OFF;
    }

    if(value & 0x02)
    {
        PL2_LED_ON;
    }
    else
    {
        PL2_LED_OFF;
    }

    if(value & 0x04)
    {
        PL3_LED_ON;
    }
    else
    {
        PL3_LED_OFF;
    }

    if(value & 0x08)
    {
        PL4_LED_ON;
    }
    else
    {
        PL4_LED_OFF;
    }

    if(value & 0x10)
    {
        PL5_LED_ON;
    }
    else
    {
        PL5_LED_OFF;
    }

}

//******************************************************************************
// Function:    void all_user_leds_off(void)
//
// Description: This routine turns off UI LEDs 1 to 6
//
// Input:       none
//
// Output:      none
//
//******************************************************************************
void all_user_leds_off(void)
{
    RED_LED_OFF;
    PL1_LED_OFF;
    PL2_LED_OFF;
    PL3_LED_OFF;
    PL4_LED_OFF;
    PL5_LED_OFF;
}


//******************************************************************************
// Function:    void Flash_Ecode(uint8 val, uint8 red_light)
//
// Description: This routine flashes val on PL1-PL5 and RED LED if TRUE
//
// Input:       none
//
// Output:      none
//
//******************************************************************************
void Flash_Ecode(uint8 val, uint8 red_light)
{
    uint8 i;
    
        for (i=0 ; i<3; i++)
        {
            if (red_light == TRUE) RED_LED_ON;
            
            display_numeric_value(val);
            sleep(1000);
            all_leds_off();
            sleep(1000);
        }
        
}
