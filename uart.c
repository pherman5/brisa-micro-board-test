/******************************************************************************
* Filename:       uart.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains the rs-232 interface functions
*                       
* List of Functions:    void init_uart(void)
*                       void serial_isr() 
*                       void poll_uart_for_messages(void)
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"
#include <stdlib.h>



#ifdef RS232_ENABLE

#define RX_BUFFER_SIZE      50
#define TX_BUFFER_SIZE      50

#define XFER_BUFFER_SIZE    50

#define UART_9600_BAUD      51

void start_tx(void);
enum 
{
    UART_IDLE = 0,
    UART_SEARCH,
    UART_MESSAGE_COMPLETE
};

static bank1 uint8 rx_ring[RX_BUFFER_SIZE];
static bank1 uint8 rx_wr_idx;
static bank1 uint8 rx_rd_idx;
static bank1 uint8 rx_full;
static bank1 uint8 rx_empty;

static bank1 uint8 tx_ring[TX_BUFFER_SIZE];
static bank1 uint8 tx_wr_idx;
static bank1 uint8 tx_rd_idx;
static bank1 uint8 tx_full;

static bank1 uint8 uart_state;

static bank1 uint8 msg_line[XFER_BUFFER_SIZE];
static bank1 uint8 msg_line_len;

/******************************************************************************
* function:    void init_uart(void)
*
* description: Initializes the UART receive ring buffer and message buffer
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void init_uart(void)
{
    uint8 idx;
    
    for(idx=0;idx<RX_BUFFER_SIZE;idx++)
    {
        rx_ring[idx] = 0;
    }
    
    for(idx=0;idx<TX_BUFFER_SIZE;idx++)
    {
        tx_ring[idx] = 0;
    }

    rx_wr_idx = 0;
    rx_rd_idx = 0;
    rx_full = FALSE;
    rx_empty = TRUE;

    tx_wr_idx = 0;
    tx_rd_idx = 0;
    tx_full = FALSE;
    
    for(idx=0;idx<XFER_BUFFER_SIZE ;idx++)
    {
        msg_line[idx] = 0;
    }    
    
    msg_line_len = 0;
    
    BAUDCON2 = 0;
    SPBRG2 = UART_9600_BAUD;
   
    TXSTA2 = 0x20;
    RCSTA2 = 0x90;

    PIE3 = PIE3 & 0xEF;
    PIE3 = PIE3 | 0x20;
    uart_state = UART_IDLE;

}
/******************************************************************************
* function:    void serial_isr() 
*
* description: Interrupt service routine that updates the receive ring buffer 
*              (if not full) with the character just received over the UART.
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void serial_isr(void) 
{   
    if(PIR3 & 0x20)
    {
        if (!rx_full)
        {
            // Room to queue the new char.
            rx_ring[rx_wr_idx] = RCREG2;

            // Look for wrap.
            if (++rx_wr_idx == sizeof(rx_ring)) 
            {
                rx_wr_idx = 0;
            }

            // Look for fullness.
            if (rx_wr_idx == rx_rd_idx) 
            {
                rx_full = TRUE;
            }
            
            // By definition, we're no longer empty.
            rx_empty = FALSE;

            PIE3 = PIE3 & 0x20;
        }
    }
	
    if((PIR3 & 0x10) && (PIE3 & 0x10))
    {
        TXREG2 = tx_ring[tx_rd_idx];
    	    
    	if(++tx_rd_idx == sizeof(tx_ring))
    	{
            tx_rd_idx = 0;
        }    
        
        // We just sent out a character so we can't be full
        tx_full = FALSE;
        	
        // Check to see if all characters have been sent
   	if(tx_rd_idx == tx_wr_idx)
    	{
            PIE3 = PIE3 & 0xEF;
        }      
    }   	   
}
   
/******************************************************************************
* function:    uart_putc(uint8 data)
*
* description: Loads a char in the uart transmit buffer
*              
* passed:      data - char to transmit
*
* returns:     none
*
******************************************************************************/
void uart_putc(uint8 data) 
{
    if(!tx_full)
    {
        tx_ring[tx_wr_idx] = data;
        
        if(++tx_wr_idx == sizeof(tx_ring))
    	{
            tx_wr_idx = 0;
        }  
        
        if(tx_rd_idx == tx_wr_idx)
    	{
            tx_full = TRUE;
        }        
    }
}    

/******************************************************************************
* function:    void putc(uint8 data)
*
* description: Transmits a char out the uart port
*              
* passed:      data - char to transmit
*
* returns:     none
*
******************************************************************************/
void putc(uint8 data) 
{
    uart_putc(data);
    start_tx();
} 

/******************************************************************************
* function:    void put_num(uint16 data)
*
* description: Transmits a number in ascii form out the uart port
*              
* passed:      data - number to transmit
*
* returns:     none
*
******************************************************************************/
void put_num(int16 data) 
{
    uint8 c;
    uint8 idx = 0;
    
    if( data < 0 )
    {
        uart_putc('-');
        data = -data;
    }
    if(data >= 10000)
    {
        c = data / 10000;
        data = data % 10000;
        uart_putc(c + '0');                
        idx = 1;
    }

    if(data >= 1000)
    {
        c = data / 1000;
        data = data % 1000;
        uart_putc(c + '0');
        idx = 1;
    }
    else if (idx == 1)
    {
        uart_putc('0');
    }    
 
    if(data >= 100)
    {
        c = data / 100;
        data = data % 100;
        uart_putc(c + '0');
        idx = 1;
    }       
    else if (idx == 1)
    {
        uart_putc('0');
    }       
        
    if(data >= 10)
    {
        c = data / 10;
        data = data % 10;
        uart_putc(c + '0');
        idx = 1;
    }   
    else if (idx == 1)
    {
        uart_putc('0');
    }             
    
    uart_putc(data + '0');
    
    start_tx();
}    

/******************************************************************************
* function:    void start_tx(void)
*
* description: Enables the uart transmit buffer empty interrupt. This will start
*				transmitting data
*              
* passed:      none
*
* returns:     none
*
******************************************************************************/
void start_tx(void)
{
    // Enable Interrupts
    PIE3 = PIE3 | 0x10;
} 

/******************************************************************************
* function:    void put_const(const uint8 *buf)
*
* description: Sends the passed string out the uart port.
*              
* passed:      *buf - pointer to the string to transmit
*
* returns:     none
*
******************************************************************************/
void put_const(const uint8 *buf) 
{ 
    uint8 c;
    
    while ((c = *buf++))
    {
        uart_putc(c);
    }
    
    start_tx();
} 

/******************************************************************************
* function:    void uart_parse_message(void)
*
* description: Parses the character string received for a valid message and 
*              performs the required action for the parsed message.
*              
* passed:      none
*
* returns:     none
*
******************************************************************************/
void uart_parse_message(void)
{
    uint8 c1, *p;

    if (msg_line_len == 0)
    {
        return;
    }

    c1 = msg_line[0];
    p = &msg_line[2];

    /* skip whitespace */
    while (*p && *p == ' ')
    {
        p++;
    }

    switch (c1)
    {
	default:
            put_const("??\n\r");
	    break;   
    }
}

/******************************************************************************
* function:    void poll_uart_for_messages(void)
*
* description: Called from the foreground to poll the uart for new input characters.
*              
* passed:      none
*
* returns:     none
*
******************************************************************************/
void poll_uart_for_messages(void)
{
    uint8 rxchar;
    
    switch(uart_state)
    {
        case UART_IDLE:
            uart_state = UART_SEARCH;
            break;
        case UART_SEARCH:
            if(rx_empty == FALSE)
            {
                // read the character
                rxchar = rx_ring[rx_rd_idx];

                // First, look for emptiness without touching the read index.
                if ( ((rx_rd_idx + 1) % (uint8)sizeof(rx_ring)) == rx_wr_idx)
                {
                    rx_empty = TRUE;
                }

                // Then point to next position.
                rx_rd_idx = (rx_rd_idx + 1) % (uint8)sizeof(rx_ring);

                // By definition, we're no longer full.
                rx_full = FALSE;

                switch (rxchar)
                {
                    case '\r':
                        uart_state = UART_MESSAGE_COMPLETE;
                        break;
                    case '\b':
                    default:
                    if (msg_line_len < XFER_BUFFER_SIZE)
                    {
                        msg_line[msg_line_len++] = rxchar;
                        // Echo the character (disable for now)
                        //uart_putc(rxchar);
                    }
                    break;
                }

            }
            break;
        case UART_MESSAGE_COMPLETE:
            put_const("\n\r");
            // Terminate the input string
            msg_line[msg_line_len] = 0;

            // Parse the messages
            uart_parse_message();

            // Set up for a new message
            msg_line_len = 0;
            uart_state = UART_IDLE;

            break;
        default:
            break;
    }
}
#endif 
