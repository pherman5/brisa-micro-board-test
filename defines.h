#ifndef DEFINES_H_INCLUDED
#define DEFINES_H_INCLUDED
/******************************************************************************
* Filename:       defines.h
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains global definitions for use by all C modules
*                       
* List of Functions:   
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/

/******************************************************************************
* Simple Types
******************************************************************************/
typedef signed char int8;
typedef unsigned char uint8;
typedef int int16;
typedef unsigned int uint16;
typedef long int32;
typedef unsigned long uint32;
typedef unsigned char bool;
typedef struct int64_tag {
    uint16 v[4];
} int64, *int64p;


/******************************************************************************
* Global Constants
******************************************************************************/
// Feature Definition
//#define EEPROM_COMM
#define USE_CONTACTS
#define MONITOR_FLASH
#define DISABLE_FLASH
//#define RS232_ENABLE
#define USE_BOOTLOADER

/******************************************************************************
* Misc. Boolean Definitions
******************************************************************************/
#define TRUE            1
#define FALSE           0

/******************************************************************************
* Lumena Error Code Definitions
******************************************************************************/
#define NO_ERROR                                0
#define ERROR_SKIN_SENSOR_TEMP_SENSOR_FAILURE   1
#define ERROR_HP_TEMP_SENSOR_SHORTED            2
#define ERROR_FAN_TACH_STALLED                  3
#define ERROR_STT_DARK_CURRENT_TOO_HIGH         4
#define ERROR_STT_GREEN_CURRENT_TOO_LOW         5
#define ERROR_STT_AMBER_CURRENT_TOO_LOW         6
#define ERROR_CAP_VOLTAGE_TIMEOUT               7
#define ERROR_CONTACT_WRITE_FAULT               8
#define ERROR_FLASH_COUNT_WRITE_ERROR           9
#define ERROR_I2C_IDLE_FAULT          		    10
#define ERROR_AD_TIMEOUT                        11
#define ERROR_POST_PROM_CHECKSUM_FAILURE        12
#define ERROR_CAP_DATA_CORRUPT                  13
#define ERROR_COUNT_DATA_CORRUPT                14
#define ERROR_SKIN_SENSOR_CAL_DATA_CORRUPT      15
#define ERROR_SKIN_SENSOR_CAL_DATA_OUT_OF_RNG	16
#define ERROR_SELF_TEST_DETECTED_ABNORMAL_RESET 17
#define ERROR_INPUT_PARAMETER_INVALID           18
#define ERROR_I2C_DEVICE_FAULT                  19
#define BOOTLOADER_FAULT                        20
#define POST_CONTACT_FAULT                      21
#define ERROR_CONTACT_READ_FAULT                22
#define LOP_CAL_CRC_FAULT                       23
#define STT_TEMP_SENSOR_RANGE_LOW               24
#define HP_TEMP_SENSOR_RANGE_LOW                25

/******************************************************************************
* System Device Time Interval Definitions
******************************************************************************/
#define DEBOUNCE_TIME_INTERVAL       (6/GLOBAL_TIME_TICK)
#define UART_POLL_TIME_INTERVAL      (10/GLOBAL_TIME_TICK)
#define TEMP_MEAS_TIME_INTERVAL      (125/GLOBAL_TIME_TICK)
#define FAN_CHECK_TIME_INTERVAL      (1500/GLOBAL_TIME_TICK)
#define LED_SERVICE_TIME_INTERVAL    (10/GLOBAL_TIME_TICK)
#define UART_SERVICE_TIME_INTERVAL   (500/GLOBAL_TIME_TICK)

// Make sure that the maximum event time is the largest of the abover intervals
#define MAX_EVENT_TIME               FAN_CHECK_TIME_INTERVAL

/******************************************************************************
* Enumerations
******************************************************************************/
// Main System States
enum 
{
    MAIN_STATE_INIT = 0,
    MAIN_STATE_POST,
    MAIN_STATE_CAL,
    MAIN_STATE_IDLE,
    MAIN_STATE_ACTIVATION_RELEASE,
    MAIN_STATE_IN_CONTACT,
    MAIN_STATE_READY,
    MAIN_STATE_FLASH,    
    MAIN_STATE_CHECK_MISSTRIGGER,
    MAIN_STATE_COOLDOWN,
    MAIN_STATE_WAIT_FLASH_CAP_VOLTAGE,
    MAIN_STATE_INACTIVITY_TIMEOUT,
    MAIN_STATE_ERROR,
    MAIN_STATE_WAIT,
    MAIN_STATE_SEQ1,
    MAIN_STATE_SEQ2,
    MAIN_STATE_SEQ3,
    MAIN_STATE_SEQ4,
    MAIN_STATE_POST_ERR,
};

// Tunes To Play On Beeper
enum
{
    INACTIVITY_TUNE,
    UNRECOVERABLE_ERROR,
};


// User request states
enum
{
    USER_REQUEST_NONE = 0,
    USER_REQUEST_EXPRESS,
    USER_REQUEST_SENSITIVE,
    USER_REQUEST_FLASH,
    USER_REQUEST_VERSION,
    USER_REQUEST_DIAGNOSTICS,
};

enum
{
    NOTIFY_NULL = 0,
    NOTIFY_IN_CONTACT_AND_READY,
    NOTIFY_FLASH,
    NOTIFY_CAP_REMOVED,
    NOTIFY_IDLE,
    NOTIFY_INACTIVITY,
};


// Modes
enum
{
    MODE_NORMAL = 0,
    MODE_EXPRESS,
    MODE_SENSITIVE,
    MODE_SHAM,
};

// Skin Type
enum
{
    SKIN_TYPE_1 = 0,
    SKIN_TYPE_2,
    SKIN_TYPE_3,
    SKIN_TYPE_4,
    SKIN_TYPE_5,
    SKIN_TYPE_6,
};


/******************************************************************************
* Structures
******************************************************************************/
// Cartridge Data Information
typedef struct gen2_cart_data_info_s
{
    uint32 flashCount;					// Current Flash Count
    uint16 cartridgeMistriggers;		// Number of Cartridge Mistriggers
    uint32 flashLimit;					// Max Number Flashes Allowed
    uint32 tubeTempComp;				// Compensation For Tube Temperature
    uint16 tubeTempCompFlashUpdate;		// Compensation For Tube Temperature
    uint8 tubeTempCompCooldown1;		// Compensation For Tube Temperature
    uint8 tubeTempCompCooldown2;		// Compensation For Tube Temperature
    uint32 express_comp;
    uint16 express_update;
    uint32 pulsePowerSumLimit;	// Power Limit // Delay After Second Pulse
    uint16 pulseFlashTime_ms;
    uint16 pulseFlashRate_ms;
    uint16 expressFlashRate_ms;
    uint8  bypassDutyCycle;
    uint32 total_flashes;
} gen2_cart_data_info_t;

// Structure Defining Brisa Read / Write Data
// Note: This Data Structure Must Not Exceed 6 Bytes In Length
typedef struct brisa_read_write_params_s
{
    uint32 flash_count;
    uint16 gen2_cartridge_mistriggers;
} brisa_read_write_params_t;

typedef struct brisa_temp_comp_params_s
{
    uint32 tube_comp;
    uint16 flash_update;
    uint8 cooldown1;
    uint8 cooldown2;
    uint32 express_comp;
    uint16 express_update;
} brisa_temp_comp_params_t;

typedef struct brisa_pulse_params_s
{
    uint32 sum_limit;
    uint16 time;
    uint16 rate;
    uint8  Duty;
    uint16 expressRate;
} brisa_pulse_params_t;

typedef struct brisa_flash_params_s
{
    uint32 flash_limit;
    uint8  spare1;
    uint8  spare2;
} brisa_flash_params_t;

typedef struct brisa_count_reset_s
{
    uint32 flashes;
    uint16 misses;
    uint32 limit;
} brisa_count_reset_t;



// Engineering and diagnostic directives
// read from the cartridge and used to modify
// the base unit behavior
typedef struct base_unit_diagnostic_params_s {
    uint8  DisableOvertemp;
    uint8  DisablePost;
    uint8  DisableSkinTypeCheck;
    uint8  ResetCounters;
    uint8  DisableSound;
    uint8  DisableContacts;
    uint8  FlashForever;
    uint8  Spare1;
    uint8  Calibrate;
} base_unit_diagnostic_params_t;



// Base unit parameters saved into the cartridge
// for diagnostic purposes
typedef struct base_unit_eeprom_vals_s {
    uint32 BaseVersionNumber;
    uint32 BaseFlashCounts;
    uint32 BaseMistriggers;
    uint32 BaseFlashLimit;
    int16  STS1_S0_Green_BCap;
    int16  STS1_S0_Amber_BCap;
    int16  STS1_S1_Green_BCap;
    int16  STS1_S1_Amber_BCap;
    int16  STS2_S0_Green_BCap;
    int16  STS2_S0_Amber_BCap;
    int16  STS2_S1_Green_BCap;
    int16  STS2_S1_Amber_BCap;
    uint8  STS1_TempAtCal_BCap;
    uint8  STS2_TempAtCal_BCap;
    int16  STS1_S0_Green_PCap;
    int16  STS1_S0_Amber_PCap;
    int16  STS1_S1_Green_PCap;
    int16  STS1_S1_Amber_PCap;
    int16  STS2_S0_Green_PCap;
    int16  STS2_S0_Amber_PCap;
    int16  STS2_S1_Green_PCap;
    int16  STS2_S1_Amber_PCap;
    uint8  STS1_TempAtCal_PCap;
    uint8  STS2_TempAtCal_PCap;
    uint32 TotalFlashes;
} base_unit_eeprom_vals_t;

typedef struct puckReflectanceData
{
    uint16  stt1_green_low;
    uint16  stt1_green_high;
    uint16  stt1_amber_low;
    uint16  stt1_amber_high;
    uint16  stt2_green_low;
    uint16  stt2_green_high;
    uint16  stt2_amber_low;
    uint16  stt2_amber_high;
    uint8   cap_type;
} puckReflectanceData_t;

typedef struct reflectanceCalcData
{
    uint16  stt1_green_sig;
    uint16  stt1_green_ref;
    uint16  stt1_amber_sig;
    uint16  stt1_amber_ref;
    uint16  stt2_green_sig;
    uint16  stt2_green_ref;
    uint16  stt2_amber_sig;
    uint16  stt2_amber_ref;
    uint16  MI_val;
    uint16  duty_cycle;
    uint8   setting;
} reflectanceCalcData_t;


typedef struct power_constants
{
    int32   g_square;
    int32   g_x;
    int32   g_offset;
    int32   a_square;
    int32   a_x;
    int32   a_offset;
    uint16  const_dc;
    uint16  limit;
} power_constants_t;

typedef struct lop_values
{
    uint16  body_std_step1;
    uint16  body_std_step2;
    uint16  body_sen_step1;
    uint16  body_sen_step2;
    uint16  body_express;
    uint16  body_sham;
    uint16  pcap_std_step1;
    uint16  pcap_std_step2;
    uint16  pcap_sen_step1;
    uint16  pcap_sen_step2;
    uint16  pcap_express;
    uint16  pcap_sham;
} lop_values_t;


/******************************************************************************
* Unions
******************************************************************************/

/******************************************************************************
* Defines
******************************************************************************/

// Processor Frequency
#define _XTAL_FREQ              32000000
#define GLOBAL_TIME_TICK        1       // Time in ms of the timer0 overflow     

// Watchdog Configuration
#define WDT_ON                  1
#define WDT_OFF                 0
#define WDT_250MS_TIMEOUT       0x10

// Skin Sensor Calibration EEPROM Address
#define EEPROM_SKIN_SENSOR_CAL_DATA_ADDRESS		0
#define EEPROM_SKIN_SENSOR_CAL_DATA_SIZE		32


// Version number EEPROM address
#define EEPROM_VERSION_ADDRESS              48
#define BOOTLOADER_STATUS_ADDRESS           52
// eeprom params
#define RCON_VALUE              62
#define RW_DATA_ADDR		64
#define	FLASH_PARAMS_ADDR	72


#define CURRENT_PWM_MIN         10
#define CURRENT_PWM_MAX         990

#define POWER_SET_160KHZ  		50

#define CAP_POLL_TIME_MS                100

#define SENSOR_SHORTED_THRESHOLD     5
#define SENSOR_OPEN_THRESHOLD       100

// A/D Conversion Channels
#define ADC_STS1_SIG                0
#define ADC_STS2_SIG                1
#define ADC_CS_MEAS                 2
#define ADC_TUBE_HV_MEAS            3
#define ADC_STS1_TEMP               4
#define ADC_STS2_TEMP               7
#define ADC_CAP_HV_MEAS             10
#define ADC_CHANNEL_MAX             ADC_CAP_HV_MEAS

// UI BEEPER Definitions
#define MIN_BEEPER_FREQUENCY        50
#define MAX_BEEPER_FREQUENCY        20000

#define CONTACT_MASK            0x000F

#define INITIAL_FAN_SPEED	100

// I2C device addresses
#define	CAP_EEPROM_DEVICE		0xA0
#define	HP_CONTACT_DEVICE		0x50

// NVRAM data storage parameters
#define PULSE_DATA_ADDR		0x0010
#define	TEMP_COMP_ADDR          0x0030
#define RESET_COUNT_VALS_ADDR   0x0040
#define	FEATURES_ADDR		0x0050
#define	MFG_DATA_ADDR		0x0060
#define	EEPROM_DATA_ADDR	0x0090
#define STANDARD_ADDR           0x00D0
#define SENSITIVE_ADDR          0x00F0
#define EXPRESS_ADDR            0x0110
#define CALIBRATION_DATA_ADDR   0x0120
#define REFLECTANCE_DATA_ADDR   0x0140

#define LOP_VALUES_ADDR         100
#define STANDARD_CAL_ADDR       100
#define SENSITIVE_CAL_ADDR      130
#define EXPRESS_CAL_ADDR        160


#define RED_LED_BLINK           0x0001
#define PL1_LED_BLINK           0x0002
#define PL2_LED_BLINK           0x0004
#define PL3_LED_BLINK           0x0008
#define PL4_LED_BLINK           0x0010
#define PL5_LED_BLINK           0x0020
#define ACT_LED_BLINK           0x0040
#define FULL_POWER_LED_BLINK    0x0080
#define EXPRESS_LED_BLINK       0x0100
#define STS_AMBER_LED_BLINK     0x0200
#define STS_GREEN_LED_BLINK     0x0400

#define BLINK_ALL_LEDS          0x0EFF
#define BLINK_USER_LEDS         0x003E  // All PLs
#define BLINK_CAP_REMOVED_LEDS  0x01BE  // All PLS + Full powr + Express

#define SYSTEM_FAULT_BLINK_TIME_ON_MSECS        250
#define SYSTEM_FAULT_BLINK_TIME_OFF_MSECS       250


/******************************************************************************
* Macros
******************************************************************************/	
#ifndef BTLDR()
#define BTLDR()	asm("ljmp 0x0000")  //Modify as needed to beginning of app
#endif

/******************************************************************************
* Global Variables
******************************************************************************/
/******************************************************************************
* Global Variables
******************************************************************************/
/* Definitions for PORTA register */
#define        EXPRESS_LED_ON       (PORTA &= ~(0x40))
#define        EXPRESS_LED_OFF      (PORTA |= 0x40)
#define        SENSITIVE_LED_ON     (PORTA &= ~(0x80))
#define        SENSITIVE_LED_OFF    (PORTA |= 0x80)

/* Definitions for PORTB register */
#define        SENSITIVE_SWITCH     ((PORTB & 0x02) ? 1 : 0)
#define        ACT_LED_ON           (PORTB &= ~(0x04))
#define        ACT_LED_OFF          (PORTB |= 0x04)
#define        PL1_LED_ON           (PORTB &= ~(0x08))
#define        PL1_LED_OFF          (PORTB |= 0x08)
#define        ACTIVATION_SWITCH    ((PORTB & 0x10) ? 1 : 0)
#define        FAN_TACH             ((PORTB & 0x20) ? 1 : 0)
#define        ICD_CLK_ON           (PORTB |= 0x40)
#define        ICD_CLK_OFF          (PORTB &= ~(0x40))
#define        ICD_DATA_ON          (PORTB |= 0x80)
#define        ICD_DATA_OFF         (PORTB &= ~(0x80))

/* Definitions for PORTC register */
#define        RED_LED_ON           (PORTC &= ~(0x01))
#define        RED_LED_OFF          (PORTC |= 0x01)
#define        PL5_LED_ON           (PORTC &= ~(0x02))
#define        PL5_LED_OFF          (PORTC |= 0x02)
#define        PL4_LED_ON           (PORTC &= ~(0x04))
#define        PL4_LED_OFF          (PORTC |= 0x04)
#define        I2C_SCL_HI           (PORTC |= 0x08)
#define        I2C_SCL_LO           (PORTC &= ~(0x08))
#define        I2C_SDA_HI           (PORTC |= 0x10)
#define        I2C_SDA_LO           (PORTC &= ~(0x10))
#define        PL3_LED_ON           (PORTC &= ~(0x20))
#define        PL3_LED_OFF          (PORTC |= 0x20)
#define        PL2_LED_ON           (PORTC &= ~(0x40))
#define        PL2_LED_OFF          (PORTC |= 0x40)
#define        CURRENT_LIMIT_HI     (PORTC |= 0x80)
#define        CURRENT_LIMIT_LO     (PORTC &= ~(0x80))

/* Definitions for PORTD register */
#define         TRIGGER_OFF         (PORTD &= ~(0x01))
#define         TRIGGER_ON          (PORTD |= 0x01)
#define         I2C_POWER_ON        (PORTD |= 0x02)
#define         I2C_POWER_OFF       (PORTD &= ~(0x02))
#define         BUCK_ENABLE         (PORTD &= ~(0x04))
#define         BUCK_DISABLE        (PORTD |= 0x04)
#define         BUZZER_HI           (PORTD |= 0x08)
#define         BUZZER_LO           (PORTD &= ~(0x08))
#define         BUZZER_STATE        ((PORTD & 0x08) ? 1 : 0)
//#define         FAN_POWER_ON        (PORTD |= 0x10)
//#define         FAN_POWER_OFF       (PORTD &= ~(0x10))
#define         EXPRESS_SWITCH      ((PORTD & 0x20) ? 1 : 0)
#define         TX_HI               (PORTD |= 0x40)
#define         TX_LO               (PORTD &= ~(0x40))
#define         RX                  ((PORTD & 0x80) ? 1 : 0)

/* Definitions for PORTE register */
#define         STS_GREEN_LED_OFF   (PORTE &= ~(0x01))
#define         STS_GREEN_LED_ON    (PORTE |= 0x01)
#define         STS_AMBER_LED_OFF   (PORTE &= ~(0x02))
#define         STS_AMBER_LED_ON    (PORTE |= 0x02)

extern base_unit_diagnostic_params_t baseDiagConfig;
extern int8 contact_status;
extern uint8 stt_flag_val;
extern uint8 i2c_fault_counter;
extern uint8 post_error_log[32];
extern uint8 error_indx;

/*********************************************************************
Function Prototypes
**********************************************************************/
extern const uint32 firmwareVer;

// adc.c
extern void adc_init(void);
extern uint8 adc_read_channel(uint8 channel, uint8 *val);
extern uint8 adc12_read_channel(uint8 channel, int16 *val, bool delay);
extern uint8 read_sts_temp(uint8 channel, int32 *temp_val);
extern uint8 read_cap_charge(int16 *charge_val);

// cartridge.c
extern uint16 ccitt(uint8 *data, uint8 len);
extern uint8 eepromReadBlock(uint8 device, uint16 blockAddress, uint8 *data, uint16 blockSize, bool do_crc );
extern uint8 cartridge_update_base_params(base_unit_eeprom_vals_t base_params);
extern uint8 load_cap_params( gen2_cart_data_info_t *data);
extern void store_uint32(uint8 device, uint16 addr, uint32 val);
extern uint8 cartridge_update_reflectance_values(reflectanceCalcData_t ref_params);


// eeprom.c
extern uint8 load_flash_params( gen2_cart_data_info_t *data);
extern uint8 eeprom_update_flash_count( uint32 flashCount, uint16 mistriggers, uint32 total_flashes);
extern uint8 reset_flash_params(void);
extern uint32 read_base_software_ver(void);
extern uint8 read_onboard_block(uint16 addr, uint8 *data, uint8 size, bool do_crc);


// utils.c
extern void utils_enable_interrupts(void);
extern void utils_disable_interrupts(void);
extern uint8 transitionState(uint8 current_state, uint8 nextState);
extern void reboot(void);

// notification.c
extern void notification( uint8 notificationID, uint16 data);

//post.c
extern void init_contact_regs(void);
extern uint8 post(void);
extern uint8 init_contacts(void);
extern uint16 post_check_i2c_devices(void);

//skin_sensor.c
extern uint8 skin_sensor_scan(uint8 mode, uint8 *pass, uint16 *power_level_duty, uint16 *life_increment);
extern uint8 read_skin_sensor_data( int16 *sts1_green_val, int16 *sts1_amber_val,
        int16 *sts2_green_val, int16 *sts2_amber_val);
extern void skin_sensor_cal(void);
extern uint8 load_skin_sensor_cal_data( void);
extern uint8 load_and_store_base_parameters(void);
extern uint8 load_power_constants(void);
extern uint8 load_puck_data(void);
extern uint8 check_stt(void);

// state.c
extern void main_state_init(void);
extern void main_state_machine(void);
extern void state_unfixable_error(uint8 error_code);
extern void main_idle_entry(void);
extern void main_cap_wait_entry(void);
extern uint32 get_flash_count(void);
extern uint32 get_flash_limit(void);
extern uint32 get_miss_count(void);
extern uint32 get_total_flashes(void);
extern uint8 do_skin_sensor_scan(void);


// timer.c
extern void timers_init(void);
extern bool timer0_overflow_check(void);
extern void init_custom_timer(uint16 timer_ticks);
extern bool custom_timer_overflow_check(void);
extern void ccp_modules_init(void);
extern void start_buzzer(uint16 frequency);
extern void stop_buzzer(void);
extern void start_led_pwm(uint8 duty_cycle);
extern void update_led_pwm(uint8 duty_cycle);
extern void stop_led_pwm(void);
extern uint8 current_pwm_start(uint8 freq, uint16 duty_cycle);
extern void current_pwm_stop(void);
extern uint32 get_msecs_since_reset(void);
extern void sleep( int16 time_ms);
extern void timeCriticalSection( uint8 isCritical);


// i2c.c
extern void i2cInit( void);
extern uint8 i2cDevicePresent( uint8 address, bool *present);
extern uint8 i2cEepromWrite(uint8 device, uint16 address, uint8 data);
extern uint8 i2cEepromByteRead(uint8 device, uint16 address, uint8 *data_byte);
extern uint8 contactWrite( uint8 reg, int8 data);
extern uint8 contactRead( uint8 reg, int8 *reg_data);
extern uint8 contactSwitchState(bool force_read, bool *contact_state,uint8 *reg_value);
//extern void i2cEepromOpenStream(uint8 device, uint16 address);
//extern uint8 i2cEepromReadStream( void);
//extern void i2cEepromCloseStream( void);

// ui.c
extern void ui_init(void);
extern void ui_beep_tune(uint8 tuneID, uint8 repeat);
extern void ui_beep_tune_stop( void);
extern void ui_update_beeper(void);
extern void ui_reset_unit_not_used_expiration_time(void);
extern uint8 ui_unit_not_used_time_expired(void);
extern uint8 getUserRequest( uint8 current_state);
extern void start_led_blink(uint16 led_mask, uint16 on_time_ms, uint16 off_time_ms);
extern void stop_led_blink(void);
extern void start_led_pulse(uint16 rise_time_ms, uint16 fall_time_ms);
extern void stop_led_pulse(void);
extern void start_led_sequence(uint16 sequence_time_ms);
extern void stop_led_sequence(void);
extern void blink_led_handler(void);
extern void all_leds_off(void);
extern void blink_leds_state(bool state);
extern void all_user_leds_off(void);
extern void display_numeric_value(uint8 value);
extern void Flash_Ecode(uint8 val, uint8 red_light);

// uart.c
extern void serial_isr(void);
extern void init_uart(void);
extern void put_num(uint16 data);
extern void put_const(const uint8 *buf);
extern void poll_uart_for_messages(void);


#endif // DEFINES.H_INCLUDED
