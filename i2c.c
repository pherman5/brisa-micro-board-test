//******************************************************************************
// Company:     Copyright (C) 2012, SHASER, Inc.
//
// Project:     Arlington
//
// Module:      i2c.c
//
// Author:      John Uccello - Boston Atlantic Technology, Inc.
//
// Description: This module contains the implementation of all routines that are
//              responsible for accessing the i2c bus.
//
// Revision History:
//
//******************************************************************************
#include "xc.h"
#include "defines.h"

//******************************************************************************
// Local Definitions
//******************************************************************************

//******************************************************************************
// Local Variables
//******************************************************************************
static uint16 idle_wait_counts = 0;
static uint8 i2c_status;

uint8 i2c_fault_counter;
//******************************************************************************
// Function Prototypes
//******************************************************************************
static uint8 i2cWaitForIdle(void);
static uint8 i2cStart(void);
static uint8 i2cRepStart(void);
static uint8 i2cStop(void);
static uint8 i2cWrite(int8 i2cWriteData);
static uint8 i2cRead(bool ack, int8 *value);

//******************************************************************************
// Function:    void i2cInit( void)
//
// Description: This routine initializes the Cartridge subsystem.
//
// Input:       none
//
// Output:      none
// 
//******************************************************************************

void i2cInit(void)
{
    //******************************
    // Initialize I2C Interface
    //******************************
    TRISC3 = 1; // Set SCL As An Input
    TRISC4 = 1; // Set SDA As An Inout

    SSPCON1 = 0x28; // Set I2C To Master Mode
    SSPCON2 = 0x00;

    SSPADD = 19; // 400K at 32M


    SSPSTATbits.CKE = 1; // Use I2C Levels

    SSPSTATbits.SMP = 1; // Disable Slew Rate Control

    PIR1bits.SSPIF = 0; // Clear SSPIF interrupt flag
    PIR2bits.BCLIF = 0; // Clear bus collision flag

}


//******************************************************************************
// Function:    bool i2cDevicePresent( uinr8 address)
//
// Description: This routine determines if a cartridge is present or not.
//
// Input:       none
//
// Output:      TRUE or FALSE depensding on whehter a cartridge is found.
// 
//******************************************************************************

uint8 i2cDevicePresent(uint8 address, bool *present)
{
//#ifdef EEPROM_COMM

    uint8 status = NO_ERROR;
    uint8 done = 0;
    uint8 retries = 0;

    while (!done && (retries < 4))
    {
        status = i2cStart();
        if (status == NO_ERROR)
        {
            status = i2cWrite(address);
        }

        if (status == NO_ERROR)
        {
            status = i2cStop();
        }

        if (status == NO_ERROR)
        {
            done = 1;
        } 
        else
        {
            retries++;
        }
    }

    if (!done)
    {
        return (ERROR_I2C_IDLE_FAULT);
    }
    // Function Returns TRUE If Transmission Is Acknowledged
    *present = !SSPCON2bits.ACKSTAT;
    return ( NO_ERROR);
//#else
//    *present = TRUE;
//    return ( NO_ERROR);
//#endif
}


//******************************************************************************
// Function:    void i2cEepromWrite(uint8 device, uint16 address, uint8 data)
//
// Description: This routine writes to the cartridge.
//
// Input:       address - The Address to write data to.
//              data - The Data to write.
//
// Output:      none
// 
//******************************************************************************

uint8 i2cEepromWrite(uint8 device, uint16 address, uint8 data)
{
#define WRITE_DELAY_TIME_MSECS          11

    



    __delay_ms(WRITE_DELAY_TIME_MSECS);
    return (NO_ERROR);
}

//******************************************************************************
// Function:    uint8 i2cEepromByteRead(uint8 device, uint16 address)
//
// Description: This routine reads from the cartridge.
//
// Input:       address - The Address to read data from.
//
// Output:      data - The data read from the cartridge.
// 
//******************************************************************************

uint8 i2cEepromByteRead(uint8 device, uint16 address, uint8 *data_byte)
{
    int8 data;
    uint8 status = NO_ERROR;
    uint8 retries = 0;
    uint8 done = 0;


    while (!done && (retries < 4))
    {
        status = i2cStart();
        if (status == NO_ERROR)
        {
            status = i2cWrite(device);
        }

        if (status == NO_ERROR)
        {
            status = i2cWrite((uint8) (address >> 8));
        }

        if (status == NO_ERROR)
        {
            status = i2cWrite((uint8) (address & 0xFF));
        }

        if (status == NO_ERROR)
        {
            status = i2cRepStart();
        }

        if (status == NO_ERROR)
        {
            status = i2cWrite((device + 1));
        }

        if (status == NO_ERROR)
        {
            status = i2cRead(FALSE, (int8 *)&data);
        }

        if (status == NO_ERROR)
        {
            status = i2cStop();
        }

        if (status == NO_ERROR)
        {
            done = 1;
        } 
        else
        {
            retries++;
        }
    }

    if (!done)
    {
        return (ERROR_I2C_IDLE_FAULT);
    }
    *data_byte = data;
    return ( NO_ERROR);
}

//******************************************************************************
// Function:    uint8 contactWrite( uint8 reg, uint8 data)
//
// Description: This routine writes to the contact chip.
//
// Input:       reg - The register to write data to.
//              data - The Data to write.
//
// Output:      Status
// 
//******************************************************************************

uint8 contactWrite(uint8 reg, int8 data)
{
    uint8 status = NO_ERROR;
    uint8 done = 0;
    uint8 retries = 0;

    while (!done && (retries < 4))
    {
        status = i2cStart();
        if (status == NO_ERROR)
        {
            status = i2cWrite(HP_CONTACT_DEVICE);
        }

        if (status == NO_ERROR)
        {
            status = i2cWrite(reg);
        }

        if (status == NO_ERROR)
        {
            status = i2cWrite(data);
        }

        if (status == NO_ERROR)
        {
            status = i2cStop();
        }

        if (status == NO_ERROR)
        {
            done = 1;
        } 
        else
        {
            retries++;
        }
    }

    if (!done)
    {
        return (ERROR_I2C_IDLE_FAULT);
    }

    return (NO_ERROR);
}

//******************************************************************************
// Function:    uint8 contactRead(uint8 reg, uint8 *reg_data)
//
// Description: This routine reads from the contact chip.
//
// Input:       reg - The register to read data from.
//              *reg_data - Pointer to load the register value into
//
// Output:      Status
// 
//******************************************************************************

uint8 contactRead(uint8 reg, int8 *reg_data)
{
    int8 read_data;
    uint8 done = 0;
    uint8 retries = 0;
    uint8 status = NO_ERROR;

    while (!done && (retries < 4))
    {
        status = i2cStart();
        if (status == NO_ERROR)
        {
            status = i2cWrite(HP_CONTACT_DEVICE);
        }

        if (status == NO_ERROR)
        {
            status = i2cWrite(reg);
        }

        if (status == NO_ERROR)
        {
            status = i2cRepStart();
        }

        if (status == NO_ERROR)
        {
            status = i2cWrite(HP_CONTACT_DEVICE + 1);
        }

        if (status == NO_ERROR)
        {
            status = i2cRead(FALSE, &read_data);
        }

        if (status == NO_ERROR)
        {
            status = i2cStop();
        }

        if (status == NO_ERROR)
        {
            done = 1;
        } 
        else
        {
            retries++;
        }

    }

    if (!done)
    {
        return (ERROR_I2C_IDLE_FAULT);
    }
    *reg_data = read_data;
    return (NO_ERROR);
}


//******************************************************************************
// Function:    uint8 contactSwitchState(bool force_read, bool *contact_state, uint8 *reg_value)
//
// Description: This routine reads the state of the contact sensor.
//
// Input:       force_read - force an update read of the contact chip register
//              *contact_state - set to true if in contact, false if not in contact
//              *reg_value - value on the chips contact register, used in the POST to determine faults
//
// Output:      Status.
// 
//******************************************************************************

uint8 contactSwitchState(bool force_read, bool *contact_state, uint8 *reg_value)
{
    uint8 io_value;
    bool ret_val;

    if (force_read == TRUE)
    {
        i2c_status = contactRead(0x03, (int8 *)&io_value);
//        if (i2c_status == NO_ERROR)
//        {
//            i2c_status = contactWrite(0x00, 0x40);
//        }
        if (i2c_status != NO_ERROR)
        {
            return (i2c_status);
        }
        contact_status = io_value;
    } 
    else
    {
        io_value = contact_status;
    }

    if ((io_value & CONTACT_MASK) == CONTACT_MASK)
    {
        ret_val = TRUE;
    } 
    else
    {
        ret_val = FALSE;
    }

    *reg_value = io_value;
    *contact_state = ret_val;
    return (NO_ERROR);
}


//******************************************************************************
// Function:    uint8 i2cWaitForIdle( void)
//
// Description: This routine waits for I2C to go idle.
//
// Input:       none
//
// Output:      none
// 
//******************************************************************************

#define IDLE_WAIT_LOOPS	250

static uint8 i2cWaitForIdle(void)
{
    uint8 ret_val = NO_ERROR;

    idle_wait_counts = 0;
    while (((SSPCON2 & 0x1F) || SSPSTATbits.R_nW) &&
            (idle_wait_counts < IDLE_WAIT_LOOPS))
    {
        CLRWDT();
        asm("NOP");
        asm("NOP");
        asm("NOP");
        idle_wait_counts++;
    }

    if (idle_wait_counts >= IDLE_WAIT_LOOPS)
    {
        ret_val = ERROR_I2C_IDLE_FAULT;
    }
    else
    {
        i2c_fault_counter = 0;
    }
    
    return ( ret_val);

}

//******************************************************************************
// Function:    uint8 i2cStart( void)
//
// Description: This routine starts the I2C transaction.
//
// Input:       none
//
// Output:      none
// 
//******************************************************************************

static uint8 i2cStart(void)
{
    uint8 ret_val = NO_ERROR;

    ret_val = i2cWaitForIdle();
    if (ret_val == NO_ERROR)
    {
        SSPCON2bits.SEN = 1;
    }

    return ( ret_val);
}

//******************************************************************************
// Function:    void i2cRepStart( void)
//
// Description: This routine is used for repeat start transactions.
//
// Input:       none
//
// Output:      none
// 
//******************************************************************************

static uint8 i2cRepStart(void)
{
    uint8 ret_val = NO_ERROR;

    ret_val = i2cWaitForIdle();
    if (ret_val == NO_ERROR)
    {
        SSPCON2bits.RSEN = 1;
    }

    return (ret_val);
}

//******************************************************************************
// Function:    uint8 i2cStop( void)
//
// Description: This routine stops the I2C transaction.
//
// Input:       none
//
// Output:      none
// 
//******************************************************************************

static uint8 i2cStop(void)
{
    uint8 ret_val = NO_ERROR;

    ret_val = i2cWaitForIdle();
    if (ret_val == NO_ERROR)
    {
        SSPCON2bits.PEN = 1;
    }

    return ( ret_val);
}

//******************************************************************************
// Function:    uint8 i2cWrite( uint8 i2cWriteData)
//
// Description: This routine writes data over the I2C.
//
// Input:       The data to write.
//
// Output:      TRUE or FALSE acknowledgement.
// 
//******************************************************************************

static uint8 i2cWrite(int8 i2cWriteData)
{
    uint8 ret_val = NO_ERROR;

    ret_val = i2cWaitForIdle();
    if (ret_val == NO_ERROR)
    {
        SSPBUF = i2cWriteData;
    }

    return ( ret_val);
}

//******************************************************************************
// Function:    uint8 i2cRead( bool ack)
//
// Description: This routine reads data from the I2C.
//
// Input:       The value to set the acknowledge to.
//
// Output:      The data read from the I2C transaction.
// 
//******************************************************************************

static uint8 i2cRead(bool ack, int8 *value)
{
    uint8 i2cReadData;
    uint8 status = NO_ERROR;

    status = i2cWaitForIdle();
    if (status == NO_ERROR)
    {
        SSPCON2bits.RCEN = 1;
    }

    if (status == NO_ERROR)
    {
        status = i2cWaitForIdle();
    }

    if (status == NO_ERROR)
    {
        i2cReadData = SSPBUF;
        status = i2cWaitForIdle();
    }

    if (status == NO_ERROR)
    {
        if (ack)
        {
            SSPCON2bits.ACKDT = 0;
        } 
        else
        {
            SSPCON2bits.ACKDT = 1;
        }

        // Send Acknowledge Sequence
        SSPCON2bits.ACKEN = 1;
        *value = i2cReadData;
    }

    return ( status);
}

