/******************************************************************************
* Filename:       main.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains the main() subroutine that is accessed at power-on
*                   reset.
*                       
* List of Functions:   main()
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"


#pragma config CONFIG1L = 0x11
#pragma config CONFIG1H = 0x18
#pragma config CONFIG2L = 0x00
#pragma config CONFIG2H = 0x1A
#pragma config CONFIG3H = 0x80
#pragma config CONFIG4L = 0x81
#pragma config CONFIG5L = 0x0F
#pragma config CONFIG5H = 0xC0
#pragma config CONFIG6L = 0x0F
#pragma config CONFIG6H = 0xE0
#pragma config CONFIG7L = 0x0F
#pragma config CONFIG7H = 0x40


const uint32 firmwareVer @ 0x7FE0 = 10100;

#define MAX_SENSOR_SHORTED_COUNT     80

#define FAN_EDGE_COUNT               2
#define FAN_STALL_LIMIT              6


//#define TIMING_TEST					0

volatile uint16 count_ms;

//static uint8 status;
static uint8 rcon_val;


__EEPROM_DATA(0,0,0,0,0x00,0x00,0,0);   // Brick Data
__EEPROM_DATA(0,0,0,0,0x00,0x00,0,0);   // Copy Of Brick Data
__EEPROM_DATA(0,0,0,0,0,0,0,0);		    // Skin Sensor Calibration Data
__EEPROM_DATA(0,0,0,0,0,0,0,0);		    // Skin Sensor Calibration Data Continued
__EEPROM_DATA(0,0,0,0x01,0,0,0,0);		// misc data
__EEPROM_DATA(0,0,0,0,0,0,0,0);			// misc data continued
__EEPROM_DATA(0,0,0x27,0x74,0,0,0,0);	// bootloader version
__EEPROM_DATA(0,0,0,0,0,0,0,0);			// misc data continued
__EEPROM_DATA(0,0,0,0,0,0,0x10,0x0E);			// misc data continued
__EEPROM_DATA(0x00,0x01,0x86,0xA0,0x00,0x00,0xE3,0x7C);			// misc data continued


static volatile uint8 cart_present;
// Configuration Parameters Read From Cartridge
base_unit_diagnostic_params_t baseDiagConfig;

/******************************************************************************
* function:    void main( void )
*
* description: This function is placed at the reset vector (0x0000) which forces 
*              program operation here at power-up. 
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void main(void)
{
    OSCCON = 0x60;	// 32 MHz Operation
    OSCCON2 = 0x00;
    OSCTUNE = 0x40;

   // Disable PORTF Analog inputs first then set TRISF
    ANCON0 = 0x9F;
    ANCON1 = 0x04;
    // Initialize IO pins for input/output operation as detailed above
    TRISA = 0x3F;
    TRISB = 0x33;
    TRISC = 0x18;
    TRISD = 0xA0;
    TRISE = 0x04;

#ifdef USE_BOOTLOADER
    rcon_val = eeprom_read(62);
#else
    rcon_val = RCON;
#endif

    ICD_CLK_OFF;
    ICD_DATA_OFF;
    // Turn off the power supply, simmer supply and enable circuit control of IGBT
    BUCK_DISABLE;
    TRIGGER_OFF;
    I2C_POWER_ON;
    TX_HI;
    
    // Keep the current limit PWM output low
    CURRENT_LIMIT_LO;
    RCON &= 0x3F;
    STKPTR &= 0x3F;
    
    INTCON = 0;
//    INTCON2 = 0x80;
    INTCON2 = 0x00;
    WPUB = 0x32;
    INTCON3 = 0;
    PIE1 = 0;
    PIE2 = 0;
    PIE3 = 0;
    PIE4 = 0;
    PIE5 = 0;
//    PIE6 = 0;
    CTMUCONH = 0;
    CCP2CON = 0;
    
    error_indx = 0;

    //Initialize our drivers
    timers_init();	   
    ccp_modules_init();
    i2cInit();
    adc_init();
    ui_init();

#ifdef RS232_ENABLE
    init_uart();
#endif

    all_leds_off();
    STS_GREEN_LED_OFF;
    STS_AMBER_LED_OFF;

    count_ms = 0;
    
    utils_enable_interrupts();	// enable global interrupts
    
    if( !(rcon_val & 0x08) )
    {
        state_unfixable_error(ERROR_SELF_TEST_DETECTED_ABNORMAL_RESET);
    }

#ifdef EEPROM_COMM
    if( eepromReadBlock(CAP_EEPROM_DEVICE, FEATURES_ADDR, (uint8 *)&baseDiagConfig,
            sizeof(base_unit_diagnostic_params_t), TRUE ) != NO_ERROR )
    {
        baseDiagConfig.DisableOvertemp = FALSE;
        baseDiagConfig.DisablePost = FALSE;
        baseDiagConfig.DisableSkinTypeCheck = FALSE;
        baseDiagConfig.ResetCounters = FALSE;
        baseDiagConfig.DisableSound = FALSE;
        baseDiagConfig.DisableContacts = FALSE;
        baseDiagConfig.FlashForever = FALSE;
        baseDiagConfig.Spare1 = FALSE;
        baseDiagConfig.Calibrate = FALSE;
    }
#else
    baseDiagConfig.DisableOvertemp = FALSE;
    baseDiagConfig.DisablePost = FALSE;
    baseDiagConfig.DisableSkinTypeCheck = TRUE;
    baseDiagConfig.ResetCounters = FALSE;
    baseDiagConfig.DisableSound = FALSE;
    baseDiagConfig.DisableContacts = TRUE;
    baseDiagConfig.FlashForever = FALSE;
    baseDiagConfig.Spare1 = FALSE;
    baseDiagConfig.Calibrate = FALSE;
#endif

    main_state_init();
    
    timeCriticalSection( FALSE);

    CLRWDT();
    WDTCON = WDT_ON | WDT_250MS_TIMEOUT;

    ui_reset_unit_not_used_expiration_time();

    current_pwm_start( POWER_SET_160KHZ, 200);
    
    ICD_CLK_OFF;
    // Execute Main Processing Loop
    while(1)
    {
        // timer is used to time schedule system events
        if (timer0_overflow_check() == TRUE)
        {                                                                                
            CLRWDT();                                
            count_ms++;

            if(count_ms % LED_SERVICE_TIME_INTERVAL == 0)
            {
                blink_led_handler();
            }

            main_state_machine();
            

#ifdef RS232_ENABLE
            if(count_ms % UART_SERVICE_TIME_INTERVAL == 0)
            { 
                poll_uart_for_messages();
            }
#endif
            // Test For Reset Of Time Checking Counter
            if(count_ms % MAX_EVENT_TIME == 0)
            {
                count_ms = 0;                                                        
            }

        }                 

 
    }
}

