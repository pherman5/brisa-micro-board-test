//******************************************************************************
// Company:     Copyright (C) 2012, SHASER, Inc.
//
// Project:     Visage
//
// Module:      notification.c
//
// Author:      John Uccello - Boston Atlantic Technology, Inc.
//
// Description: This module contains the implementation of all routines that are
//              responsible for user interface notifications.
//
// Revision History:
//
//******************************************************************************
#include "xc.h"
#include "defines.h"

//******************************************************************************
// Local Definitions
//******************************************************************************

#define IN_CONTACT_AND_READY_DELAY_TIME_MSECS   100

#define CAL_DONE_DELAY_TIME_MSEC                500

#define RESET_NOTIFICATION_ID                   0xFF

//******************************************************************************
// Local Variables
//******************************************************************************
static uint8 lastNotificationID = RESET_NOTIFICATION_ID;
static uint16 lastData=255;

//******************************************************************************
// Function Prototypes
//******************************************************************************

//******************************************************************************
// Function:    void notification( uint8 notificationID, uint16 data)
//
// Description: This routine presents the appropriate UI notification to the
//              user.
//
// Input:       notificationID - ID of notification to present to user
//              data - Miscellaneous data
//
// Output:      none
// 
//******************************************************************************
void notification( uint8 notificationID, uint16 data)
{
    //**************************************
    // Prevent Multiple Repeat Notifications
    // Of The Same Type
    //**************************************
    if( (notificationID == lastNotificationID) && (data == lastData))
    {
        return;
    }

    lastNotificationID = notificationID;
    lastData = data;

    switch( notificationID)
    {
        case NOTIFY_NULL:
            break;

        case NOTIFY_IN_CONTACT_AND_READY:
            start_led_pulse(1500, 1500);
            break;

        case NOTIFY_FLASH:
            break;

        case NOTIFY_CAP_REMOVED:
            all_leds_off();
            break;

        case NOTIFY_IDLE:
            stop_led_blink();
            all_user_leds_off();
            RED_LED_OFF;
            start_led_blink(STS_AMBER_LED_BLINK,500,500);
            break;
            
        case NOTIFY_INACTIVITY:
            all_user_leds_off();
            start_led_blink(RED_LED_BLINK,250,250);
            ui_beep_tune(INACTIVITY_TUNE, TRUE);
            break;

        default:
            break;
    }
}
