/******************************************************************************
* Filename:       eeprom.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains all functions associated with interfacing to the 
*                   internal EEPROM on the 16F1939 microcntroller
*                       
* List of Functions:  
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"

// Function Prototypes

// Local Variables

/******************************************************************************
* function:    uint8 eeprom_update_flash_count( uint32 flashCount, uint16 mistriggers)
*
* description: Writes the flash and miss trigger counts to the cartridge menory.
*
* passed:      flashCount - number of flashes to write
*			   mistriggers - number of miss triggers to write
*
* returns:     status
*
******************************************************************************/
uint8 eeprom_update_flash_count( uint32 flashCount, uint16 mistriggers, uint32 total_flashes)
{
uint8 i;
uint16 crc_val;
uint8 write_buf[20];
uint8 status=NO_ERROR;

    write_buf[0] = (uint8)((flashCount >> 24) & 0x00ff);
    write_buf[1] = (uint8)((flashCount >> 16) & 0x00ff);
    write_buf[2] = (uint8)((flashCount >> 8) & 0x00ff);
    write_buf[3] = (uint8)(flashCount & 0x00ff);

    write_buf[4] = (uint8)((mistriggers >> 8) & 0x00ff);
    write_buf[5] = (uint8)(mistriggers & 0x00ff);

    crc_val = ccitt( write_buf, 6);
    write_buf[6] = (uint8)(crc_val & 0x00ff);
    write_buf[7] = (uint8)(crc_val >> 8);

    for(i = 0; i < 8; i++ )
    {
        eeprom_write(RW_DATA_ADDR+i,write_buf[i]);
    }

    write_buf[0] = (uint8)((total_flashes >> 24) & 0x00ff);
    write_buf[1] = (uint8)((total_flashes >> 16) & 0x00ff);
    write_buf[2] = (uint8)((total_flashes >> 8) & 0x00ff);
    write_buf[3] = (uint8)(total_flashes & 0x00ff);


    for(i = 0; i < 4; i++ )
    {
        eeprom_write(130+i,write_buf[i]);
    }

    return(status);
}


/******************************************************************************
* function:    uint8 reset_flash_params(void)
*
* description: Sets the flash count miss count and flash limit
*               values in eeprom to the values in the cap memory.
*
* passed:      None
*
* returns:     status
*
******************************************************************************/
uint8 reset_flash_params(void)
{
uint8 i;
uint16 crc_val;
uint8 write_buf[20];
uint8 status=NO_ERROR;
uint32 flash_count,flash_limit,total_flashes;
uint16 miss_count,flash_rate;
uint8 status;
brisa_count_reset_t reset_count_data;

    status = eepromReadBlock(CAP_EEPROM_DEVICE, RESET_COUNT_VALS_ADDR, (uint8 *)&reset_count_data,
            sizeof(brisa_count_reset_t), TRUE);

    if( status == NO_ERROR)
    {
        flash_count = reset_count_data.flashes;
        miss_count = reset_count_data.misses;
        flash_limit = reset_count_data.limit;
        flash_rate = 0;
        total_flashes = 0;
    }
    else
    {
        flash_count = 0;
        miss_count = 0;
        flash_limit = 100000;
        flash_rate = 0;
        total_flashes = 0;
    }
    eeprom_update_flash_count( flash_count, miss_count, total_flashes);


    write_buf[0] = (uint8)(flash_limit >> 24);
    write_buf[1] = (uint8)(flash_limit >> 16);
    write_buf[2] = (uint8)(flash_limit >> 8);
    write_buf[3] = (uint8)(flash_limit);

    write_buf[4] = (uint8)(flash_rate >> 8);
    write_buf[5] = (uint8)(flash_rate);

    crc_val = ccitt( write_buf, 6);

    write_buf[6] = (uint8)(crc_val >> 8);
    write_buf[7] = (uint8)(crc_val);


    for(i = 0; i < 8; i++ )
    {
        eeprom_write(FLASH_PARAMS_ADDR+i,write_buf[i]);
        CLRWDT();
    }

    return(status);
}

