/******************************************************************************
* Filename:       post.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains the POST functionality
*                       
* List of Functions:   uint8 post(void)
*
* Special Notes:    
*  
* Revision History:
* 29 Jan 2009  Initial version
*
******************************************************************************/
#include "xc.h"
#include "defines.h"

#define _OFFSET 0
#define _ALGORITHM 2     
#define _RESULT_WIDTH 2

#ifdef USE_BOOTLOADER
#define _START  0x1000
#else
#define _START  0x0000
#endif

#define _END    0x7FFF

uint16 sum;
uint32 addr;


static uint8 i2c_status;
static bool present;
static uint8 contact_state;
static uint8 contact_reg_value;

/******************************************************************************
* function:    void init_contact_regs(void)
*
* description: canned contact register setup. Only used if no cap eeprom is available
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void init_contact_regs(void)
{
#ifdef USE_CONTACTS
    contactWrite(0x1f,0x0f);
    contactWrite(0x21,0x0f);
    contactWrite(0x2a,0x00);
    contactWrite(0x27,0x00);
    contactWrite(0x24,0x28);
    contactWrite(0x00,0x40);
#endif
}


/******************************************************************************
* function:    uint8 post(void)
*
* description: The power-on self test performs the following measurements:
*              - Checksum of program memory to verify the contents 
*
* passed:      none
*
* returns:     0 - successful
*              1 - failed
*
******************************************************************************/
uint8 post(void)                        
{
uint16 check_sum;
uint8 flash_val;
uint32 addr_val;


    if( baseDiagConfig.DisablePost == FALSE)
    {
        CLRWDT();
        WDTCON = WDT_OFF | WDT_250MS_TIMEOUT;
        // ***************************************************************************
        // Perform a checksum of program memory
        // ***************************************************************************



        addr_val=0x7FFe;
        ReadFlash(addr_val,1,&flash_val);
        check_sum = (uint16)((uint16)flash_val << 8);
        addr_val=0x7FFF;
        ReadFlash(addr_val,1,&flash_val);
        check_sum |= (uint16)flash_val;

        uint16 flash_word;
        addr = _START;	// point to start of checksum range
    	sum = _OFFSET;
    	while(addr < (_END - _RESULT_WIDTH))
        {
            CLRWDT();
            ReadFlash(addr+1,1,&flash_val);
            flash_word = (uint16)((uint16)flash_val << 8);
            ReadFlash(addr,1,&flash_val);
            flash_word |= (uint16)flash_val;

            sum += flash_word;		// read and accumulate a word
            addr += 2;	// select address for next loop iteration
    	}

        if(sum != check_sum)
        {
            WDTCON = WDT_ON | WDT_250MS_TIMEOUT;
            return(ERROR_POST_PROM_CHECKSUM_FAILURE);
        }  
        i2c_status = post_check_i2c_devices();
        if( i2c_status != NO_ERROR )
        {
            WDTCON = WDT_ON | WDT_250MS_TIMEOUT;
            return(i2c_status);
        }


    }


    if( present == TRUE )
    {
        i2c_status = init_contacts();
        if( i2c_status != NO_ERROR )
        {
            return(i2c_status);
        }
    }
	// Turn on the watchdog timer
    CLRWDT();  
    WDTCON = WDT_ON | WDT_250MS_TIMEOUT;

    CLRWDT();  

    return(NO_ERROR);
}    


/******************************************************************************
* function:    uint8 init_contacts(void)
*
* description: routine to initialize the contact chip registers
*              from a table in cap eeprom
*
* passed:      none
*
* returns:     status
*
******************************************************************************/
uint8 init_contacts(void)
{

        // initialize the contact chip

        i2c_status = contactSwitchState(TRUE, &contact_state, &contact_reg_value);
        if(i2c_status != NO_ERROR )
        {
            return(i2c_status);
        }

        i2c_status = contactSwitchState(TRUE, &contact_state, &contact_reg_value);
        if(i2c_status != NO_ERROR )
        {
            return(i2c_status);
        }

        if( ((contact_reg_value & CONTACT_MASK) != 0) || (contact_state == TRUE) )
        {
            return(POST_CONTACT_FAULT);
        }


    return(NO_ERROR);

}

/******************************************************************************
* function:    uint16 post_check_i2c_devices(void)
*
* description: routine to ping all the i2c devices for presence
*
* passed:      none
*
* returns:     status
*
******************************************************************************/
uint16 post_check_i2c_devices(void)
{

    i2c_status = i2cDevicePresent(HP_CONTACT_DEVICE, &present);
    if( i2c_status != NO_ERROR )
    {
        return(i2c_status);
    }
    if( !present )
    {
        return(ERROR_I2C_DEVICE_FAULT);
    }

    return(NO_ERROR);

}