/******************************************************************************
* Filename:       timer.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains the functions associated with timer0 (the system  
*                   time base), timer1 (used for the capture of the AC line
*                   frequency) and timer2 (used for PWM output)
*                       
* List of Functions:   timers_init()
*                      void interrupt general_isr(void)
*                      bool timer0_overflow_check(void)
*                      void init_custom_timer(uint16 timer_ticks)
*                      bool custom_timer_overflow_check(void)
*					   ccp_modules_init()
*                      void current_pwm_start(uint8 freq, uint16 duty_cycle)
*                      uint32 get_msecs_since_reset(void)
*                      void sleep( int16 time_ms)
*                                         
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"

//******************************************************************************
// System Clock Setup For 1 ms Interrupt Period
// To Obtain a 1 ms (1000 Hz) Timer With A Resident 32 MHz Clock
// Set The Prescaler To 32 Such That The timer0 Clock Is (32 MHz/4)/32 = 250 KHz
// This Requires 250 Counts For A 1 ms (1000 Hz) Timer
// Preload Value = 256 - 250 = 6
//******************************************************************************
#define TIMER0_PRELOAD      6

#define TIMER4_ON       0x4
//#define TIMER4_4X_PRE   0x1

/******************************************************************************
* Local Variables
******************************************************************************/
uint32 msecs_since_reset;
uint8 criticalSection;

static bool timer0_overflow;
static uint8 sound_timer_hi;
static uint8 sound_timer_lo;
static uint8 led_timer_hi;
static uint8 led_timer_lo;
static uint8 led_duty_cycle;
static uint8 led_counter;


/******************************************************************************
* function:    void timers_init(void)
*
* description: Initializes System Timers
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void timers_init(void)
{   
    // Initialize Critical Section Variable
    criticalSection = FALSE;
    uint16 timer_val;

    INTCON = INTCON | 0x20;

    T0CON = 0xC4;
    // TIMER 0
    // Initializes timer0 as a free running counter with an interrupt
    // generated on overflow.
    TMR0L = TIMER0_PRELOAD;
        
    // Clear the 1ms status flag
    timer0_overflow = FALSE;

    // Initialize System Time Counter
    msecs_since_reset = 0;

    // Timer 1 used for LED PWM
    T1GCON = 0x00;
    T1CON = 0x32;
    // set for 50K interrupt
    timer_val = 65515;
    led_timer_hi = (uint8)(timer_val >> 8);
    led_timer_lo = (uint8)timer_val;

    // TIMER 4
    // Initializes timer4 to be used for PWM generation
    T4CON = 0x00;

    // TIMER 3
    // Initializes timer3 to be used to tone generation
    T3GCON = 0x00;
    T3CON = 0x32;
}

/******************************************************************************
* function:    void interrupt general_isr(void)
*
* description: Interrupt routine to service timer0 overflow.
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void interrupt general_isr(void)
{
    // Check the interrupt flags to determine the interrupt source

    // Timer 3
    if( PIR1 & 0x01 )
    {
        led_counter++;
        if( led_counter == led_duty_cycle )
        {
            ACT_LED_OFF;
        }

        if( led_counter == 100 )
        {
            led_counter = 0;
            ACT_LED_ON;
        }

        TMR1H = led_timer_hi;
        TMR1L = led_timer_lo;
        PIR1 &= 0xFE;
    }

    // Timer 3
    if( PIR2 & 0x02 )
    {
        if(BUZZER_STATE == TRUE)
        {
            BUZZER_LO;
        }
        else
        {
            BUZZER_HI;
        }
        TMR3H = sound_timer_hi;
        TMR3L = sound_timer_lo;
        PIR2 &= 0xFD;
    }

    if(INTCON & 0x04)
    {
        ICD_DATA_ON;
        INTCON &= 0xFB;
        TMR0 = TIMER0_PRELOAD;
        // allways turn off fet
	if( criticalSection == FALSE)
	{	
            // Update Beeper
            ui_update_beeper();

	    timer0_overflow = TRUE;
            ICD_DATA_OFF;
        }

	// Update Time for Milli Secs Since Reset
        msecs_since_reset += GLOBAL_TIME_TICK;
    }
    
#ifdef RS232_ENABLE
    serial_isr();
#endif
}

/******************************************************************************
* function:    bool timer0_overflow_check(void)
*
* description: Returns TRUE/FALSE describing whether timer0 has overflowed
*
* passed:      none
*
* returns:     true if timer0 overflowed, FALSE otherwise
*
******************************************************************************/
bool timer0_overflow_check(void)
{  
    if(timer0_overflow == TRUE)
    {
        timer0_overflow = FALSE;
        return(TRUE);
    }
    else
    {
        return(FALSE);
    }
}

/******************************************************************************
* function:    void start_buzzer(uint16 frequency)
*
* description: Sets timer 3 up to interrupt at twice the desired tone frequency.
*
* passed:      frequency - Desired frequency
*
* returns:     none
*
******************************************************************************/
void start_buzzer(uint16 frequency)
{
    uint16 timer_val;
    uint32 temp_val;

    temp_val = 1000000 / ((uint32)frequency * 2);
    timer_val = (uint16)(65535 - temp_val);

    sound_timer_hi = (uint8)(timer_val >> 8);
    sound_timer_lo = (uint8)(timer_val);

    utils_disable_interrupts();
    TMR3H = sound_timer_hi;
    TMR3L = sound_timer_lo;
    PIE2 = PIE2 | 0x02;
    T3CON = T3CON | 0x01;
    utils_enable_interrupts();

}

/******************************************************************************
* function:    void stop_buzzer(void)
*
* description: Turns off timer 3 and disables the timer 3 interrupt.
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void stop_buzzer(void)
{
    utils_disable_interrupts();
    PIE2 = PIE2 & 0xFD;
    T3CON = T3CON & 0xFE;
    BUZZER_LO;
    utils_enable_interrupts();

}

/******************************************************************************
* function:    void start_led_pwm(uint8 duty_cycle)
*
* description: Sets timer 1 up to interrupt at 10K to PWM the activation LED.
*
* passed:      duty_cycle -
*
* returns:     none
*
******************************************************************************/
void start_led_pwm(uint8 duty_cycle)
{
    utils_disable_interrupts();
    TMR1H = led_timer_hi;
    TMR1L = led_timer_lo;
    led_duty_cycle = duty_cycle;
    led_counter = 0;
    ACT_LED_ON;
    PIE1 = PIE1 | 0x01;
    T1CON = T1CON | 0x01;
    utils_enable_interrupts();

}

/******************************************************************************
* function:    void update_led_pwm(uint8 duty_cycle)
*
* description: Update the duty cycle for the activation switch LED.
*
* passed:      duty_cycle - Desiresd duty cycel
*
* returns:     none
*
******************************************************************************/
void update_led_pwm(uint8 duty_cycle)
{
    utils_disable_interrupts();
    led_duty_cycle = duty_cycle;
    led_counter = 0;
    if( duty_cycle != 0)
    {
        ACT_LED_ON;
    }
    else
    {
        ACT_LED_OFF;
    }
    utils_enable_interrupts();

}

/******************************************************************************
* function:    void led_pwm(void)
*
* description: Turns off timer 1 and disables the timer 1 interrupt.
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void stop_led_pwm(void)
{
    utils_disable_interrupts();
    PIE1 = PIE1 & 0xFE;
    T1CON = T1CON & 0xFE;
    ACT_LED_OFF;
    utils_enable_interrupts();

}


/******************************************************************************
* function:    void ccp_modules_init(void)
*
* description: Initializes the CCPR modules to the particular time base to use
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void ccp_modules_init(void)
{
    // CCP4 - current PWM uses timer 4
    // If we do fan PWM will be timer 1/2
    CCPTMRS = 0x08;
}    


/******************************************************************************
* function:    uint8 current_pwm_start(uint8 freq, uint16 duty_cycle)
*
* description: Starts a PWM signal through CCP1 to adjust the current level 
*              through the tube during a flash. This control signal is used to 
*              compensate for tube variations, tube aging effects and line voltage
*              affects on output flash level. The input parameters specify the 
*              frequency and duty cycle of the signal produced.
*
* passed:      freq - frequency of the generated signal. 
*              duty cycle  - duty cycle of the PWM signal scaled by 10X percent
*                            (Minimum - 10, Maximum 990)
*
* returns:     Status
*
******************************************************************************/
uint8 current_pwm_start(uint8 freq, uint16 duty_cycle)
{
    uint16 duty_value;
    
    if((duty_cycle < CURRENT_PWM_MIN) || (duty_cycle > CURRENT_PWM_MAX))
    {
        return(ERROR_INPUT_PARAMETER_INVALID);
    }    
    
    PR4 = freq;
    
    // Enable single output pwm mode and steer it to PWMD
    CCP4CON = 0x0C;
      
    duty_value = (uint16)((4 * (uint32)freq) * (uint32)duty_cycle / 1000);
    // fix for 10 bit
    CCPR4L = (uint8)(duty_value >> 2);

    duty_value = (duty_value << 4) & 0x30;
    CCP4CON |= (uint8)duty_value;
    // Set the PWM delay to 0
    PWM1CON = 0;
    
    T4CON |= TIMER4_ON;

    return(NO_ERROR);
}

/******************************************************************************
* function:    void current_pwm_stop(void)
*
* description: Stops the current control PWM.
*
* passed:      None
*
* returns:     None
*
******************************************************************************/
void current_pwm_stop(void)
{
    T4CON = T4CON & 0xFB;
    CCP4CON = 0x00;
    CURRENT_LIMIT_LO;
}



/******************************************************************************
* function:    uint32 get_msecs_since_reset(void)
*
* description: Returns Number Of Milli Seconds Since Reset
*
* passed:      none
*
* returns:     msecs_since_reset - milli seconds since reset
*
******************************************************************************/
uint32 get_msecs_since_reset(void)
{
    return( msecs_since_reset);
}

/******************************************************************************
* function:    void sleep( int16 time_ms)
*
* description: Sleeps Specified Number Of Milliseconds
*
* passed:      int time_ms - Time To Sleep In msecs.
*
* returns:     none
*
******************************************************************************/
void sleep( int16 time_ms)
{
    uint32 timeThreshold = msecs_since_reset + time_ms;

    while( msecs_since_reset < timeThreshold)
    {
        CLRWDT();
    }
}

/******************************************************************************
* function:    void timeCriticalSection( uint8 isCritical)
*
* description: Set a critical section for interrupt processing.
*
* passed:      isCritical - TRUE or FALSE
*
* returns:     none
*
******************************************************************************/
void timeCriticalSection( uint8 isCritical)
{
    if( isCritical)
        criticalSection = TRUE;
    else
        criticalSection = FALSE;
}

