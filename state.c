/******************************************************************************
* Filename:       state.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains the main state machine of the unit
*                       
* List of Functions:   void main_state_init(void)
*                      void main_state_machine(state_data_t *sd)
*                      void state_unfixable_error(uint8 error_code)
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"


#define IGBT_HI_THRESHOLD_COUNTS            163     //80A  (across 0.04 ohms)
#define IGBT_NUM_HI_THRESHOLD_READINGS      10

#define IGBT_LO_THRESHOLD_COUNTS            196       //6A   (across 0.04 ohms)
#define IGBT_NUM_LO_THRESHOLD_READINGS      10

// Definitions For Tube Temperature Compensation 
#define TUBE_TEMP_COMP_COUNT_MAX            60000
#define TUBE_TEMP_COMP_COUNT_LEVEL_SWITCH   20000

#define FAN_PWM_UPDATE_TIME_MS              2000
#define CONTACT_UPDATE_TIME_MS              80

#define FLASH_V_I_STORAGE_SIZE              100

#define STT_SCAN_RATE_MS                    500

#define SHAM_DUTY_CYCLE                     55

//#define FLASH_CAP_VOLTAGE_LOW_THRESHOLD           2866 // 350V
//#define FLASH_CAP_VOLTAGE_LOW_THRESHOLD           3071 // 375V
//#define FLASH_CAP_VOLTAGE_FULL_THRESHOLD          3276 // 400V
//#define FLASH_CAP_OVERVOLTAGE_THRESHOLD           3439 // 420V
//#define FLASH_CAP_MISSTRIGGER_THRESHOLD           3194 // 390V

#define MODE_BUTTON_WAIT_TIME_MS        1000

//#define CONTACT_ADJ_DELAY                   6000    // wait X msec to adjust thresholds
#define CONTACT_ADJ_DELAY                   4000    // wait X msec to adjust thresholds
#define NO_CONTACT_THRESH_ADJ_CYCLES        3       // number of times CONTACT_ADJ_DELAY elapse b4 re-adj
//#define LOW_THRESH                          28      // lowest threshold contact setting
#define LOW_THRESH                          16      // lowest threshold contact setting
#define HIGH_THRESH                         96      // highest threshold contact

#define LEDON_DELAY_TIME        1000
#define LEDOFF_DELAY_TIME       1000

//static void state_increment_temp_comp_count(void);
static void adjust_threshold(void);
static void sequence_lights(void);

int8 contact_status;
uint8 stt_flag_val = 0;  // 0-stt LEDs OFF; 1-green ON, amber OFF; 2-amber ON green OFF;
uint8 post_error_log[32];
uint8 error_indx = 0;

static uint8 main_state;
//static gen2_cart_data_info_t cartData;
//static uint32 percent_left;

//static uint32 tube_temp_comp_count;
static uint8 i2c_status;
//static bool present;
static bool Post_Done;

static uint32 contact_poll_time_msec=0;
static uint8 user_request;


static bool contact_read_state;
static uint8 contact_reg_state;

//static uint8 skin_sensor_status;
static uint8 post_error_status;

static bool express_button_wait=FALSE;
static uint32 express_button_wait_time;
static bool sensitive_button_wait=FALSE;
static uint32 sensitive_button_wait_time;

//static uint8 led_duty = 20;


static int8 cx_delta[4];
static int8 cx_threshold[4];
static uint8 no_threshold_adj[4];
static uint32 adj_threshold_time = 0;

extern uint8 bypass_duty_val;


//static uint8 adjust_counter;  //for rs232
static uint8 test_count = 0;
static int8 reg_val;

/******************************************************************************
* function:    void main_state_init(void)
*
* description: Initializes the variables associated with the main state machine
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void main_state_init(void)
{
    uint8 i;
    // Reset Tube Temperature Compensation Parameter

    // Enter Main State Under Normal Conditions 
    main_state = MAIN_STATE_INIT;
    main_state = transitionState(main_state,MAIN_STATE_INIT);


    // If Cap Is Detected To Be Off When Entering This State Machine,
    // Set A Flag To Handle Transition To The Cap Removed State.

    for(i=0;i < 4; i++)
    {
        no_threshold_adj[i] = 0;
    }
    adj_threshold_time = get_msecs_since_reset() + CONTACT_ADJ_DELAY;


//    led_duty = 20;
//    adjust_counter = 0;
    error_indx = 0;
    Post_Done = FALSE;

//    i2cEepromWrite(CAP_EEPROM_DEVICE,0xCFFE,(uint8)(data_index << 8));
//    i2cEepromWrite(CAP_EEPROM_DEVICE,0xCFFF,(uint8)data_index);
  
}

/******************************************************************************
* function:    void main_state_machine(state_data_t *sd)
*
* description: Contains the main state machine which dictates the flow of the  
*              product. The flow is dictated by the events passed into the 
*              function which include the present switch states, the past
*              switch states and the latest hand piece and base unit temperature
*              readings.
*
* passed:      sd->sw_curr - current state of all the switches and sensors in the 
*              unit (ui switches, activation button, contact sensors, door 
*              ajar sensor)
*              sd->sw_prev - previous state of all the switches and sensors in the 
*              unit (ui switches, activation button, contact sensors, door 
*              ajar sensor)         
*              sd->hp_temp - latest hand piece temperature reading
*              sd->base_temp - latest base unit temperature reading
*              which requires putting the unit in the unrecoverable state
*              required.
*
* returns:     none
*
******************************************************************************/
void main_state_machine(void)
{
    uint16 i;
    uint8 stat;
    int32 temp_val;

    user_request = getUserRequest(main_state);


    if( (user_request == USER_REQUEST_EXPRESS) && (express_button_wait == FALSE ))
    {
        express_button_wait = TRUE;
        express_button_wait_time = get_msecs_since_reset();
    }

    if((user_request == USER_REQUEST_EXPRESS) && (main_state == MAIN_STATE_WAIT))
    {
        main_state = transitionState(main_state,MAIN_STATE_SEQ1);
    } 
    else 
    {
        if((user_request == USER_REQUEST_EXPRESS) && (main_state == MAIN_STATE_SEQ4))
        {
            main_state = transitionState(main_state,MAIN_STATE_POST_ERR);
        }
        else
        {
            // Toggle comfort mode
            if( (user_request == USER_REQUEST_EXPRESS) &&
                    (get_msecs_since_reset() > (express_button_wait_time + MODE_BUTTON_WAIT_TIME_MS)) &&
                    (main_state != MAIN_STATE_INACTIVITY_TIMEOUT) )
            {


                main_state = transitionState(main_state,MAIN_STATE_IDLE);
            }
        }
    }

    if( (user_request == USER_REQUEST_SENSITIVE) && (sensitive_button_wait == FALSE ))
    {
        sensitive_button_wait = TRUE;
        sensitive_button_wait_time = get_msecs_since_reset();
    }


    if((user_request == USER_REQUEST_SENSITIVE) && (main_state == MAIN_STATE_SEQ2))
    {
        main_state = transitionState(main_state,MAIN_STATE_SEQ3);
    } 
    else
    {
        // Toggle comfort mode
        if((user_request == USER_REQUEST_SENSITIVE) &&
                (get_msecs_since_reset() > (sensitive_button_wait_time + MODE_BUTTON_WAIT_TIME_MS)) &&
               /*(main_state != MAIN_STATE_COOLDOWN) && */(main_state != MAIN_STATE_INACTIVITY_TIMEOUT) )
        {




            main_state = transitionState(main_state,MAIN_STATE_IDLE);
        }
    }
    

    if( ui_unit_not_used_time_expired() == TRUE )
    {
        main_state = transitionState(main_state,MAIN_STATE_INACTIVITY_TIMEOUT);
    }

    if( (main_state != MAIN_STATE_INIT) && (get_msecs_since_reset() >= contact_poll_time_msec) )
    {
#ifdef USE_CONTACTS
        i2c_status = contactRead(0x03,&contact_status);
        if( i2c_status == NO_ERROR )
        {
            i2c_status = contactWrite(0x00,0x40);
        }
        
        if( i2c_status != NO_ERROR )
        {
            state_unfixable_error(i2c_status);
        }
        
#else
        contact_status = CONTACT_MASK;
#endif

#ifdef RS232_ENABLE
        adjust_counter++;
        if(  adjust_counter >= 12 )
        {
            for( i = 0; i < 4; i++ )
            {
                CLRWDT();
                i2c_status = contactRead(0x10+i,&cx_delta[i]);
                put_num((int16)cx_delta[i]);
                put_const("  ");
                i2c_status = contactRead(0x30+i,&cx_threshold[i]);
                put_num((int16)cx_threshold[i]);
                if( i != 3 )
                {
                    put_const("  ");
                }
            }
            put_const("\r\n");
            adjust_counter = 0;
        }
#endif

        if (get_msecs_since_reset() > adj_threshold_time)
        {
            adjust_threshold();
        }

	contact_poll_time_msec = get_msecs_since_reset() + CONTACT_UPDATE_TIME_MS;

        // Check to see if the base or hand piece is too hot
    }

    switch(main_state)
    {
        case MAIN_STATE_INIT:
            Post_Done = FALSE;
            all_leds_off();
            sequence_lights();
            sleep(500);
            main_state = transitionState(main_state,MAIN_STATE_WAIT);
            EXPRESS_LED_ON;
            break;

        case MAIN_STATE_WAIT:
            if( user_request == USER_REQUEST_FLASH )
                main_state = transitionState(main_state, MAIN_STATE_INIT);
        break;    

        case MAIN_STATE_SEQ1:
            EXPRESS_LED_OFF;
            stt_flag_val = 0;  // Dark current
            stat = check_stt();
            sleep(500);
            stt_flag_val = 1;  // green ON amber OFF
            stat = check_stt();
            sleep(500);
            stt_flag_val = 2;  // green OFF amber ON
            stat = check_stt();
            sleep(500);
            main_state = transitionState(main_state, MAIN_STATE_SEQ2);
            break;
            
        case MAIN_STATE_SEQ2:
            SENSITIVE_LED_ON;
            break;

        case MAIN_STATE_SEQ3:
            SENSITIVE_LED_OFF;
            stat = contactRead(0x1f,&reg_val);
            if (stat != NO_ERROR)
            {
                state_unfixable_error(stat);
            }
            if (test_count == 0)
            {
                if (reg_val != 0x2f)  // default on first pass
                {
                    state_unfixable_error(ERROR_CONTACT_READ_FAULT);
                    Flash_Ecode(ERROR_CONTACT_READ_FAULT,TRUE);
                }
                test_count++;
            } else
            {
                if (reg_val != 0x0f)  // written value after initial pass
                {
                    state_unfixable_error(ERROR_CONTACT_READ_FAULT);
                    Flash_Ecode(ERROR_CONTACT_READ_FAULT,TRUE);
                }
            }
            contactWrite(0x1f,0x0f);
            stat = contactRead(0x1f,&reg_val);
            if (stat != NO_ERROR)
            {
                state_unfixable_error(stat);
            }
            if (reg_val != 0x0f)
            {
                state_unfixable_error(ERROR_CONTACT_WRITE_FAULT);
                Flash_Ecode(ERROR_CONTACT_WRITE_FAULT,TRUE);
            }
            i2c_status = read_sts_temp(ADC_STS1_TEMP, &temp_val);
            if( i2c_status != NO_ERROR )
            {
                state_unfixable_error(i2c_status);
                Flash_Ecode(i2c_status,TRUE);
            } else
                if(temp_val < 10)  // temp should be > 50 C
                {
                    state_unfixable_error(STT_TEMP_SENSOR_RANGE_LOW);
                    Flash_Ecode(STT_TEMP_SENSOR_RANGE_LOW,TRUE);
                }
            i2c_status = read_sts_temp(ADC_STS2_TEMP, &temp_val);
            if( i2c_status != NO_ERROR )
            {
                state_unfixable_error(i2c_status);
                Flash_Ecode(i2c_status,TRUE);
            } else
                if(temp_val < 10)
                {
                    state_unfixable_error(HP_TEMP_SENSOR_RANGE_LOW);
                    Flash_Ecode(HP_TEMP_SENSOR_RANGE_LOW,TRUE);
                }
            main_state = transitionState(main_state, MAIN_STATE_POST);
            break;

        case MAIN_STATE_SEQ4:
            if( user_request == USER_REQUEST_FLASH )
                main_state = transitionState(main_state, MAIN_STATE_INIT);
            break;
            
        case MAIN_STATE_POST_ERR:
            if(error_indx > 0)
            {
                start_led_blink(EXPRESS_LED_BLINK,100,100); // indicate error detected
                for ( i=0; i < error_indx; i++)  // show error in seq
                {
                    display_numeric_value(post_error_log[i]);
                    sleep(3000);  // 3 sec wait
                    display_numeric_value(0); // all off
                    sleep(1000);
                }
                main_state = transitionState(main_state, MAIN_STATE_SEQ4);
                ACT_LED_ON;
            }
            else
            {
                start_led_pulse(1500, 1500);
                main_state = transitionState(main_state, MAIN_STATE_IDLE);
                Post_Done = TRUE;
            }
            break;

        case MAIN_STATE_POST:
        // do post if ok check for cal. If cal goto cal state if not goto idle state
            // Test For Cartridge Presence - Do Not Run POST If Cap Is Removed
            // Check For A Problem Communicating With The Cartridge And Load In Skin Sensor Cartridge Data
            // Faulty Or Missing Cartridge Will Result In Failure Here

            // Cartridge Is Present And Cap Is In Place, Therefore, Run POST
            // Perform Power On Tests
            post_error_status = post();
            if(post_error_status != NO_ERROR)
            {
                // Unrecoverable Error Condition
                state_unfixable_error( post_error_status);
            }
            main_state = transitionState(main_state,MAIN_STATE_POST_ERR);
        break;

        case MAIN_STATE_CAL:
            // do calibration and stay here
        break;


        case MAIN_STATE_IDLE:                                                                                 
            // Alert Operator That Contacts Are In Touch With Skin Surface
            i2c_status = contactSwitchState(FALSE,&contact_read_state,&contact_reg_state);

            if( i2c_status != NO_ERROR )
            {
                state_unfixable_error(i2c_status);
            }

//            if( (contact_read_state == TRUE) || ((baseDiagConfig.DisableContacts  == TRUE) && (baseDiagConfig.DisableSkinTypeCheck == TRUE)) )
//            {
//                    // Signal In Contact With Skin / Ready To Flash
//                    sleep(50);
//                    main_state = transitionState(main_state,MAIN_STATE_IN_CONTACT);
//                    ui_reset_unit_not_used_expiration_time();
//            }
//
//            state_decrement_temp_comp_count();
            if (user_request == USER_REQUEST_FLASH)
            {
                    main_state = transitionState(main_state,MAIN_STATE_ACTIVATION_RELEASE);
                    ui_reset_unit_not_used_expiration_time();
            }
            else
            {
                notification(NOTIFY_NULL,0);
            }

            break;

        case MAIN_STATE_ACTIVATION_RELEASE:
            // on button release go into in contact state
            if(( user_request == USER_REQUEST_NONE ) && (Post_Done == FALSE))
            {
                main_state = transitionState(main_state,MAIN_STATE_INIT);
            }
            else
                main_state = transitionState(main_state,MAIN_STATE_IDLE);

            break;

        case MAIN_STATE_IN_CONTACT:
            // contact ok, do the sts check and set PL
//            skin_sensor_status = do_skin_sensor_scan();
//            stt_scan_time_ms = get_msecs_since_reset() + STT_SCAN_RATE_MS;
            main_state = transitionState(main_state,MAIN_STATE_READY);
            break;


        case MAIN_STATE_READY:

            // contact and sts ok, wait for activation button or loss of contact
            i2c_status = contactSwitchState(FALSE,&contact_read_state,&contact_reg_state);

            if( i2c_status != NO_ERROR )
            {
                state_unfixable_error(i2c_status);
            }

        if( (contact_read_state == FALSE) && (baseDiagConfig.DisableContacts  == FALSE) )
        {
                main_state = transitionState(main_state,MAIN_STATE_IDLE);
        }

            ui_reset_unit_not_used_expiration_time();

            break;

        case MAIN_STATE_INACTIVITY_TIMEOUT:
            i2c_status = contactSwitchState(FALSE,&contact_read_state,&contact_reg_state);
            if( i2c_status != NO_ERROR )
            {
                state_unfixable_error(i2c_status);
            }
            if( (user_request != USER_REQUEST_NONE) || (contact_read_state == TRUE) )
            {
                ui_beep_tune_stop();
                ui_reset_unit_not_used_expiration_time();
                main_state = transitionState(main_state,MAIN_STATE_IDLE);
            }

            break;

        default:
            break;

/******************************************************************************
* function:    void state_increment_temp_comp_count(void)
*
* description: Increments the temperature compensation count of the system.
*              Constants assume that this is called on the 6ms tick of the state machine.
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
//static void state_increment_temp_comp_count(void)
//{
//    if( express_mode == TRUE )
//    {
//        tube_temp_comp_count += cartData.express_update;
//    }
//    else
//    {
//        tube_temp_comp_count += cartData.tubeTempCompFlashUpdate;
//    }
//    
//    if(tube_temp_comp_count > TUBE_TEMP_COMP_COUNT_MAX)
//    {
//        tube_temp_comp_count  = TUBE_TEMP_COMP_COUNT_MAX;
//    }    
//}

/******************************************************************************
* function:    void state_decrement_temp_comp_count(void)
*
* description: Decrements the temperature compensation count of the system.
*              Constants assume that this is called on the 6ms tick of the state machine.
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
    
    
    // Decrement the tube temperature compensation count                        
    }         
}                                                               

/******************************************************************************
* function:    void state_unfixable_error(uint8 error_code)
*
* description: Called when the unit determines there is an unfixable error and
*              operation should be shut down
*
* passed:      error_code - Error Code Causing Unfixable Condition
*
* returns:     none
*
******************************************************************************/
void state_unfixable_error(uint8 error_code)
{
//    uint16 count_ms;
    uint8 j, match;
//    bool display_code = FALSE;
    utils_enable_interrupts();	// enable global interrupts
    // put bus reset stuff here
    
    if( error_code == ERROR_I2C_IDLE_FAULT)
    {
        i2c_fault_counter++;
        if( i2c_fault_counter < 4)
        {
            I2C_POWER_OFF;
            sleep(500);
            I2C_POWER_ON;
            sleep(100);
            i2cInit();
            init_contacts();
            return;
        }
    }
    
//    count_ms = 0;
    match = 0;

    if(error_indx == 0)
    {
        post_error_log[error_indx] = error_code;
        error_indx++;
    } else
    {
        if( (error_indx > 0) && (error_indx < 32))
        {
            for(j=0; j < error_indx; j++)
            {
                if (post_error_log[j] == error_code) // don't log same error
                {
                    match = TRUE;
                }
            }
        }
        
        if ((match == FALSE) && (error_indx < 32))
            {
                post_error_log[error_indx] = error_code;
                error_indx++;
            }
    }
}


/******************************************************************************
* function:    void main_idle_entry(void)
*
* description: entry function for MAIN_STATE_IDLE state
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void main_idle_entry(void)
{    
    all_user_leds_off();
    stop_led_pulse();
}

/******************************************************************************
* function:    void main_cap_wait_entry(void)
*
* description: entry function for MAIN_STATE_WAIT_FLASH_CAP_VOLTAGE state
*
* passed:      none
*
* returns:     none
*
******************************************************************************/


/******************************************************************************
* function:    uint32 get_flash_count(void)
*
* description: Returns the current flash count
*
* passed:      none
*
* returns:     flash count
*
******************************************************************************/

/******************************************************************************
* function:    uint32 get_flash_limit(void)
*
* description: Returns the flash limit
*
* passed:      none
*
* returns:     flash limit
*
******************************************************************************/

/******************************************************************************
* function:    uint32 get_miss_count(void)
*
* description: Returns the current miss flash count
*
* passed:      none
*
* returns:     miss count
*
******************************************************************************/

/******************************************************************************
* function:    uint32 get_miss_count(void)
*
* description: Returns the current miss flash count
*
* passed:      none
*
* returns:     miss count
*
******************************************************************************/

/******************************************************************************
* function:    uint8 do_skin_sensor_scan(void)
*
* description: Routine that calls the skin tester scan
*
* passed:      none
*
* returns:     pass/fail status
*
******************************************************************************/

//stt_scan_start = get_msecs_since_reset();


//    store_uint32(CAP_EEPROM_DEVICE,0xF200,power_level_duty);
//    store_uint32(CAP_EEPROM_DEVICE,0xF204,flash_count_increment);
    
    

        // do tube temp comp

    

/******************************************************************************
* function:    void adjust_threshold(void)
*
* description: checks delta counts and adjusts threshold for each individual
*              sensor
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
static void adjust_threshold(void)
{
int8 sense_stat;
uint8 diff;
uint8 contact_bit;
uint8 i;
bool write_it;

    i2c_status = contactRead(0x03,&sense_stat);
    if( i2c_status != NO_ERROR )
    {
        state_unfixable_error(i2c_status);
    }

    contact_bit = 1;
    for( i = 0; i < 4; i++ )
    {
        CLRWDT();

        if( sense_stat & contact_bit )
        {
            i2c_status = contactRead(0x10+i,&cx_delta[i]);
            if( i2c_status == NO_ERROR )
            {
                i2c_status = contactRead(0x30+i,&cx_threshold[i]);
            }
            if( i2c_status != NO_ERROR )
            {
                state_unfixable_error(i2c_status);
            }
            write_it = FALSE;
            if (cx_delta[i] > cx_threshold[i])
            {
                diff = cx_delta[i] - cx_threshold[i];
//                if (((diff > 32) && (cx_threshold[i] < HIGH_THRESH)) ||
//                    ((diff > 16) && (cx_threshold[i] == LOW_THRESH) /*&& (LOW_THRESH == 28)*/))
                if (((diff > 16) && (cx_threshold[i] < HIGH_THRESH)) ||
                    ((diff > 8) && (cx_threshold[i] == LOW_THRESH) /*&& (LOW_THRESH == 28)*/))
                {
                    cx_threshold[i] += 4;
                    write_it = TRUE;
                }
                else if ((diff < 16) && (cx_threshold[i] > LOW_THRESH))
                {
                    cx_threshold[i] -= 4;
                    write_it = TRUE;
                }
                if(write_it == TRUE)
                {
                    i2c_status = contactWrite(0x30+i,cx_threshold[i]);
                    if( i2c_status != NO_ERROR )
                    {
                        state_unfixable_error(i2c_status);
                    }
                    no_threshold_adj[i] = 0;
                }
                else if( no_threshold_adj[i] == NO_CONTACT_THRESH_ADJ_CYCLES )
                {
                    no_threshold_adj[i] = 0;
                }
                else
                {
                    no_threshold_adj[i]++;
                }
            }
        }
        else
        {
            if((cx_threshold[i] > 16) && (no_threshold_adj[i] == NO_CONTACT_THRESH_ADJ_CYCLES))
            {
                cx_threshold[i] -= 4;
                i2c_status = contactWrite(0x30+i,cx_threshold[i]);
                if( i2c_status != NO_ERROR )
                {
                    state_unfixable_error(i2c_status);
                }
                no_threshold_adj[i] = 0;
            }
            else if( no_threshold_adj[i] == NO_CONTACT_THRESH_ADJ_CYCLES )
            {
                no_threshold_adj[i] = 0;
            }
            else
            {
                no_threshold_adj[i]++;
            }
        }
        contact_bit = contact_bit << 1;
    }
    adj_threshold_time = get_msecs_since_reset() + CONTACT_ADJ_DELAY;
}

/******************************************************************************
* function:    void sequence_lights(void)
*
* description: turns on LEDs sequentially for testing
*              
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
static void sequence_lights(void)
{
            PL1_LED_ON;
            sleep(LEDON_DELAY_TIME);
            PL1_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);
            
            PL2_LED_ON;
            sleep(LEDON_DELAY_TIME);
            PL2_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);
            
            PL3_LED_ON;
            sleep(LEDON_DELAY_TIME);
            PL3_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);
            
            PL4_LED_ON;
            sleep(LEDON_DELAY_TIME);
            PL4_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);
            
            PL5_LED_ON;
            sleep(LEDON_DELAY_TIME);
            PL5_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);

            RED_LED_ON;
            sleep(LEDON_DELAY_TIME);
            RED_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);

            STS_GREEN_LED_ON;
            sleep(LEDON_DELAY_TIME);
            STS_GREEN_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);

            STS_AMBER_LED_ON;
            sleep(LEDON_DELAY_TIME);
            STS_AMBER_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);

            EXPRESS_LED_ON;
            sleep(LEDON_DELAY_TIME);
            EXPRESS_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);

            SENSITIVE_LED_ON;
            sleep(LEDON_DELAY_TIME);
            SENSITIVE_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);

            ACT_LED_ON;
            sleep(LEDON_DELAY_TIME);
            ACT_LED_OFF;
            sleep(LEDOFF_DELAY_TIME);

            all_leds_off();

}
