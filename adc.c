/******************************************************************************
* Filename:       adc.c
* Project:        Lumena
* Written By:     A. Aaron
*
* Copyright (C) 2010, SHASER, Inc.
*
* Description:      Contains all functions associated with the a/d converter
*                       
* List of Functions:   void adc_init(void)
*                      uint8 adc_read_channel(uint8 channel, uint8 *val)
*                      uint8 adc12_read_channel(uint8 channel, uint16 *val)
*
* Special Notes:    
*  
* Revision History:
*
******************************************************************************/
#include "xc.h"
#include "defines.h"

#define AD_CHAN_SEL_MASK        0x7C
#define TIMEOUT_LOOPS             25

/******************************************************************************
* function:    void adc_init(void)
*
* description: Initializes the variables associated with the a/d drivers 
*
* passed:      none
*
* returns:     none
*
******************************************************************************/
void adc_init(void)
{    
    ADCON0 = 0x01;  // channel 0, Turn adc on
    ADCON1 = 0x00;
}

/******************************************************************************
* function:    uint8 adc12_read_channel(uint8 channel, int16 *val, bool delay)
*
* description: Selects the appropriate channel of the A/D converter and starts
*              a conversion. The full 12-bits of the A/D conversion are passed
*              back to the caller.  
*
* passed:      channel - the a/d channel on which to perform a conversion. The 
*              possible range of channel selection is 0-15.
*              val - the 12-bit result of the a/d conversion
*              delay - flag to delay for channel selection settling time
*
* returns:     0 - successful
*              1 - failed
*
******************************************************************************/
uint8 adc12_read_channel(uint8 channel, int16 *val, bool delay )
{
    uint8 loops;

    if(channel > ADC_CHANNEL_MAX)
    {
        return(ERROR_INPUT_PARAMETER_INVALID);
    }

    ADCON2 = 0xAA; // Fosc/32, right justified result
    // Select the channel
    channel &= 0x1F;
    ADCON0 &= ~(AD_CHAN_SEL_MASK);
    ADCON0 |= (channel<<2);
    
    // Allow the channel to settle
    if( delay == TRUE )
    {
        __delay_us(7);
    }

    // Start the conversion and wait for completion (~50uS) or for time out  
    // (~250us) so the system does not hang.
    GO_nDONE=1;	    
    for(loops=0;loops<TIMEOUT_LOOPS;loops++)
    {
        if(GO_nDONE == 0)
        {
            *val = ((uint16)(ADRESH))<<8;
            *val += ADRESL;
            return(NO_ERROR);
        }    

        __delay_us(10);
    }       

    return(ERROR_AD_TIMEOUT);
}



/******************************************************************************
* function:    int32 read_sts_temp(uint8 channel)
*
* description: Reads the sts temperature sensor and returns.
*               the reading in degrees C.
*
* passed:      none
*
* returns:     sts degrees C
*
******************************************************************************/
uint8 read_sts_temp(uint8 channel, int32 *temp_val)
{
uint32 temp_value;
int16 adc_value;
int32 adc_sum;
uint8 status=NO_ERROR;
uint8 i;

#define NUM_STS_TEMP_READINGS 4

    if( (channel != ADC_STS1_TEMP) &&  (channel != ADC_STS2_TEMP) )
    {
        return(ERROR_INPUT_PARAMETER_INVALID);
    }

    adc_sum = 0;
    for(i = 0; i < NUM_STS_TEMP_READINGS; i++ )
    {
        status = adc12_read_channel(channel,&adc_value,TRUE);
        if( status != NO_ERROR )
        {
            return(status);
        }
        adc_sum += adc_value;
    }

    adc_sum /= NUM_STS_TEMP_READINGS;

#define TEMP_0_READING 327

    if( adc_sum <= TEMP_0_READING )
    {
        temp_value = 0;
    }
    else
    {
        temp_value = ((adc_sum * 100) - 32760) / 1597;
    }

    *temp_val = (int32)temp_value;
    if( (temp_value > SENSOR_OPEN_THRESHOLD) || (temp_value < SENSOR_SHORTED_THRESHOLD) )
    {
        if( channel == ADC_STS1_TEMP )
        {
            return(ERROR_SKIN_SENSOR_TEMP_SENSOR_FAILURE);
        }
        else
        {
            return(ERROR_HP_TEMP_SENSOR_SHORTED);
        }
    }

    return(NO_ERROR);

}


/******************************************************************************
* function:    uint16 read_cap_charge(void)
*
* description: Reads the flash cap charge and returns
*               the reading in counts.
*
* passed:      none
*
* returns:     Flash cap voltage in CC
*
******************************************************************************/
uint8 read_cap_charge(int16 *charge_val)
{
int16 adc_value;
int32 adc_sum;
uint8 status=NO_ERROR;
uint8 i;

#define NUM_CHARGE_READINGS 8

    adc_sum = 0;

    for(i = 0; i < NUM_CHARGE_READINGS; i++ )
    {
        status = adc12_read_channel(ADC_CAP_HV_MEAS,&adc_value,TRUE);
        if( status != NO_ERROR )
        {
            return(status);
        }
        adc_sum += adc_value;
    }

    *charge_val = adc_sum / NUM_CHARGE_READINGS;

    return(NO_ERROR);

}

